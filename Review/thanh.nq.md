# HW01 - AVM
*review for NGuyễn Hữu Thắng: UC002 - Mua vé 24h, UC003 - Mua thẻ* 

- làm khá đầy đủ 
- còn chưa chi tiết phần nghiệp vụ thay thế

# HW01 - AFC 
*review for NGuyễn Hữu Thắng: UC008 - Soát vé ra 1 chiều, UC009 - Soát vé ra 24h*

- làm tốt

# HW02 - Thiết kế tổng quan
*review for NGuyễn Hữu Thắng: UC008 - Soát vé ra 1 chiều, UC009 - Soát vé ra 24h*

- rất chi tiết

# HW03 - Thiết kế chi tiết
*review for NGuyễn Hữu Thắng: thiết kế màn hình và biểu đồ chuyển màn hình*

- đầy đủ

# HW junit test
*review cho Nguyễn Hữu Thắng:*

- chưa thấy có unit test

# HW coupling & cohesion
*review cho Nguyễn Hữu Thắng*

- đã nêu ra khái niệm và 1 số trường hợp vận dụng

# HW thay đổi yêu cầu
*review cho Nguyễn Văn Thắng*

- Đã nêu ra các thay đổi cơ sở dữ liệu tương thích với yêu cầu thay đổi phù hợp với yêu cầu: vé mới, tuyến đường mới