package com.group16.afc.manager;

import com.group16.afc.database.MysqlConnect;
import com.group16.afc.model.AFC;
import com.group16.afc.model.Ticket24h;
import com.group16.afc.model.Ticket24hStatus;

import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.recognizer.TicketRecognizer;

public class AFCManagerTicket24hIn extends AFCManager {

	
	
	
	
	@Override
	public boolean validateTicket() {
		try {
			code = TicketRecognizer.getInstance().process(barcode);
			return true;
		} catch (InvalidIDException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		
	}

	@Override
	public boolean isAvailableTicket() {
		if(code != null) {
			Ticket24h t  = MysqlConnect.getInstance().getTicketTF(code);
			ticket = t;
			if (t!= null) {
				if (t.getStatus().compareTo(Ticket24hStatus.NEW) == 0) {
					
					return true;
				}
				else return false;
			}
		}
		return false;
		
	}

	@Override
	public void processTicket() {
		if (!validateTicket()) {
			System.out.println("invalid ticket");
		}else {
			if(!isAvailableTicket()) {
				System.out.println("unable ticket: "+ (Ticket24h)ticket);
			}
			else {
				System.out.println("able ticket: "+ (Ticket24h)ticket);
			}
		}
		
	}

	public static void main(String[] args) {
		AFCManagerTicket24hIn afc = new AFCManagerTicket24hIn();
		afc.setBarcode("dddddddd");
		afc.processTicket();
	}
}
