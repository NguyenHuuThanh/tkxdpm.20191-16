package utils;

import com.group16.afc.model.Station;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package utils
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description Interface of caculating Balance Method
 */
public interface CalculatingBalanceInterface {
	/**
	 * calculate balance of travel from inStaion and outStation
	 * @param inStation {@link Station}
	 * @param outStation {@link Station}
	 * @return balance of travel 
	 */
	public double calculateBalance(Station inStation, Station outStation);
}
