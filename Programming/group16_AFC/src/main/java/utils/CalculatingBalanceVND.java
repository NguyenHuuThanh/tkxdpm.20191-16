package utils;

import com.group16.afc.model.Station;
import java.lang.Math;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package utils
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description calculate balance version #1
 */
public class CalculatingBalanceVND implements CalculatingBalanceInterface {

	private static CalculatingBalanceVND sInstance = null;

	private CalculatingBalanceVND() {
	}

	public static CalculatingBalanceVND getInstance() {

		if (sInstance == null) {
			sInstance = new CalculatingBalanceVND();
		}
		return sInstance;
	}

	public double calculateBalance(Station inStation, Station outStation) {
		double balance = 0;

		double travelDistance = Math.abs(inStation.getDistance() - outStation.getDistance());

		balance = CalculatingBalanceVND.getInstance().moneyOf(travelDistance);

		return balance;
	}

	public double moneyOf(double travelDistance) {
		travelDistance = Math.abs(travelDistance);
		if (travelDistance == 0)
			return 0;
		else if (travelDistance <= 5)
			return 10000;
		else {
			double addDistance = travelDistance - 5;
			double unitMoneyAddDistance = Math.ceil(addDistance / 2.0);
			double balance = 10000 + 2000 * unitMoneyAddDistance;
			return balance;
		}
	}

}
