package config;

import com.group16.afc.database.Database;

import utils.CalculatingBalanceInterface;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package config
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description configure program when start
 */
public class Configuration {

	private static Configuration instance = null;

	private CalculatingBalanceInterface calBalance;

	private Database db;

	private Configuration() {

	}

	public static Configuration getInstance() {
		if (instance == null) {
			instance = new Configuration();
		}
		return instance;
	}

	public CalculatingBalanceInterface getCalBalance() {
		return calBalance;
	}

	public void setCalBalance(CalculatingBalanceInterface calBalance) {
		this.calBalance = calBalance;
	}

	public Database getDb() {
		return db;
	}

	public void setDb(Database db) {
		this.db = db;
	}

}
