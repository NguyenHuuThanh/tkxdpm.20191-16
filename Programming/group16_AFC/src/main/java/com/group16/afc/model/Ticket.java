package com.group16.afc.model;

import java.time.LocalDateTime;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.model
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description store data of Ticket
 */
public abstract class Ticket {
	/**
	 * id
	 */
	protected String id;
	/**
	 * time bought
	 */
	protected LocalDateTime timeBuy;
	/**
	 * code
	 */
	protected String code;
	/**
	 * newest Travel of Ticket
	 */
	protected Travel newestTravel;

	public Ticket() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Ticket(String id) {
		super();
		this.id = id;
	}

	public LocalDateTime getTimeBuy() {
		return timeBuy;
	}

	public void setTimeBuy(LocalDateTime timeBuy) {
		this.timeBuy = timeBuy;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Travel getNewestTravel() {
		return newestTravel;
	}

	public void setNewestTravel(Travel newestTravel) {
		this.newestTravel = newestTravel;
	}

	@Override
	public String toString() {
		return "Ticket [id=" + id + ", timeBuy=" + timeBuy + ", code=" + code + "]";
	}

	public boolean isCard() {
		return false;
	}

	public boolean isOneWay() {
		return false;
	}

	public boolean isOneDay() {
		return false;
	}
}
