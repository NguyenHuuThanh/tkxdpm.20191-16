package com.group16.afc.database;

import com.group16.afc.model.TicketOneWay;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.database
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description TicketOW DAO
 */
public interface DatabaseOW {
	/**
	 * get OW by _code
	 * 
	 * @param _code {@link String} is matched with barcode
	 * @return {@link TicketOneWay}
	 */
	public TicketOneWay getOW(String _code);

	/**
	 * update status of OW
	 * 
	 * @param ticketId {@link String}
	 * @param status {@link String}
	 * @return <code>true</code> if complete
	 */
	public boolean updateOWStatus(String ticketId, String status);
}
