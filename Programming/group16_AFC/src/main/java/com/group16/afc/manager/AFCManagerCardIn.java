package com.group16.afc.manager;

import java.time.LocalDateTime;
import com.group16.afc.model.PrepaidCard;
import com.group16.afc.model.PrepaidCardStatus;
import com.group16.afc.model.Travel;
import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.scanner.CardScanner;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.manager
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description Manager for PC IN
 */
public class AFCManagerCardIn extends AFCManager {

	public PrepaidCard prepaidCard;

	@Override
	public boolean validateTicket() {
		try {
			code = CardScanner.getInstance().process(barcode);
			if (code != null) {
				setState(AFCState.BARCODE_VALID);
				System.out.println(getState());
				return true;
			}

		} catch (InvalidIDException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setState(AFCState.BARCODE_INVALID);
			System.out.println(getState());
			return false;
		}
		setState(AFCState.BARCODE_INVALID);
		System.out.println(getState());
		return false;
	}

	@Override
	public boolean getInforTicket() {
		if (getState() == AFCState.BARCODE_VALID) {
			PrepaidCard t = getDatabase().getPC(code);
			System.out.println(t);
			if (t != null) {
				prepaidCard = t;
				setState(AFCState.GET_INFO_SUCCESS);
				System.out.println(getState());
				return true;
			}
		}
		setState(AFCState.GET_INFO_FAIL);
		System.out.println(getState());
		return false;
	}

	@Override
	public boolean isAvailableTicket() {
		if (getState() == AFCState.GET_INFO_SUCCESS) {
			PrepaidCard t = prepaidCard;

			if (t != null) {
				if (t.getCash() < PrepaidCard.baseBalance) {
					setState(AFCState.NOT_ENOUGH_BASE_BALANCE);
					System.out.println(getState());
					return false;
				} else {
					if (afc != null) {
						t.setNewTravelInpoint(afc.getStation());
					}
					System.out.println(t);
					// NEW is state when inPointID and outPointID = null
					if (t.getStatus() == PrepaidCardStatus.NEW || t.getStatus() == PrepaidCardStatus.END_TRAVEL) {
						prepaidCard.setStatus(PrepaidCardStatus.NEW_TRAVEL);
						Travel travel = new Travel();
						travel.setInStation(getDatabase().getStation(afc.getStation().getId()));
						t.setNewestTravel(travel);
						setState(AFCState.AVAILBLE_TICKET);
						System.out.println(getState());
						return true;
					} else {
						if (t.getStatus() == PrepaidCardStatus.NEW_TRAVEL) {
							setState(AFCState.WRONG_DIRECTION_NOT_IN);
							System.out.println(getState());
							return true;
						}
						if (t.getStatus() == PrepaidCardStatus.BLOCKED) {
							setState(AFCState.EXPRIED_TICKET);
							System.out.println(getState());
							return true;
						}
					}
				}

			}
		}
		setState(AFCState.UNAVAILBLE_TICKET);
		System.out.println(getState());
		return false;

	}

	@Override
	public boolean updateInfoTicket() {
		if (getState() == AFCState.AVAILBLE_TICKET) {
			PrepaidCard t = prepaidCard;
			if (afc != null) t.setNewTravelInpoint(afc.getStation());
			getDatabase().updatePCStatus(t.getId(), t.getStatus().toString());

			if (afc != null) getDatabase().insertNewTravel(t.getId(), afc.getStation().getId(), LocalDateTime.now());

			setState(AFCState.UPDATE_SUCCESS);
			System.out.println(getState());
			return true;
		}
		setState(AFCState.UPDATE_FAIL);
		System.out.println(getState());
		return false;
	}

	@Override
	public boolean controlGate() {
		if (getState() == AFCState.UPDATE_SUCCESS) {
			GateManager.process();
			setState(AFCState.DONE);
			System.out.println(getState());
		}
		return false;
	}

	@Override
	public void processTicket() {
		validateTicket();
		getInforTicket();
		isAvailableTicket();
		updateInfoTicket();
		controlGate();
	}
}
