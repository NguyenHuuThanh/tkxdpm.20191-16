package com.group16.afc.view;

import javax.swing.JLabel;

import com.group16.afc.manager.AFCState;
import com.group16.afc.model.TicketOneWay;
import config.Configuration;

/**
 * 
 * @author nguyen huu thang
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.view
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description display one way ticket detail screen
 */
public class OneWayPanel extends BasePanel {
	private static final int ROW_1 = 80;
	private static final int ROW_2 = 180;
	private static final int ROW_3 = 280;
	private static final int ROW_4 = 380;
	private static final int ROW_5 = 480;
	private static final int COL_1 = 50;
	private static final int COL_2 = 300;
	private static final int WDITH = 600;

	private JLabel lbID, lbTravel, lbStatus, lbBalance, lbStation;
	private JLabel title;
	private JLabel lbIDValue, lbTravelValue, lbStatusValue, lbBalanceValue, lbStationValue;

	@Override
	public void initComponents() {
		setLayout(null);
		title = setTitle("");
		lbID = setTextLabel(COL_1, ROW_1, WDITH, 80, "ID");
		lbTravel = setTextLabel(COL_1, ROW_2, WDITH, 80, "Travel");
		lbStatus = setTextLabel(COL_1, ROW_3, WDITH, 80, "Status");
		lbBalance = setTextLabel(COL_1, ROW_4, WDITH, 80, "Balance");
		lbStation = setTextLabel(COL_1, ROW_5, WDITH, 80, "Station");
		lbIDValue = setTextLabel(COL_2, ROW_1, WDITH, 80, "1");
		lbTravelValue = setTextLabel(COL_2, ROW_2, WDITH, 80, "1");
		lbStatusValue = setTextLabel(COL_2, ROW_3, WDITH, 80, "1");
		lbBalanceValue = setTextLabel(COL_2, ROW_4, WDITH, 80, "1");
		lbStationValue = setTextLabel(COL_2, ROW_5, WDITH, 80, "1");
	}

	/**
	 * display owticket detail on screen
	 * @param afcstate afc machine state
	 * @param ticket one way ticket is used
	 */
	public void setOneWayInfo(AFCState afcstate, TicketOneWay ticket) {
		String stationName = AFCGUI.getInstance().afc.getStation().getName();

		if (afcstate == AFCState.EXPRIED_TICKET) {
			title.setText("Expried Ticket");
		} else if (afcstate == AFCState.WRONG_STATION) {
			title.setText("Wrong station Check In ticket One Way");
		} else if (afcstate == AFCState.NOT_ENOUGH_BALANCE) {
			title.setText("Not enough balance");
		} else if (afcstate == AFCState.AVAILBLE_TICKET) {
			title.setText("Opening gate with one-way Ticket");
		}
		lbIDValue.setText(ticket.getId());
		lbTravelValue.setText(ticket.getInStation().getName() + " - " + ticket.getOutStation().getName());
		lbStatusValue.setText(ticket.getStatus().toString());
		StringBuilder str = new StringBuilder();
		str.append(Configuration.getInstance().getCalBalance().calculateBalance(ticket.getInStation(),
				ticket.getOutStation()));
		if (afcstate == AFCState.NOT_ENOUGH_BALANCE) str.append(" < ").append(ticket.getNewestTravel().getBalance());
		lbBalanceValue.setText(str.toString());
		// TODO set current station
		lbStationValue.setText(stationName);
	}

	@Override
	public void registerListener() {
		// TODO Auto-generated method stub

	}

	@Override
	public void addComponents() {
		// TODO Auto-generated method stub
		add(title);
		add(lbID);
		add(lbTravel);
		add(lbStatus);
		add(lbBalance);
		add(lbStation);
		add(lbIDValue);
		add(lbTravelValue);
		add(lbStatusValue);
		add(lbBalanceValue);
		add(lbStationValue);
		addBackButton();
	}
}
