package com.group16.afc.view;

import javax.swing.JLabel;

import com.group16.afc.manager.AFCState;

/**
 * 
 * @author nguyen huu thang
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.view
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description display eror screen
 */
public class InvalidPanel extends BasePanel {
	private static final int ROW_1 = 200;
	private static final int ROW_2 = 300;
	private static final int COL_1 = 280;
	private static final int COL_2 = 300;
	private static final int WDITH = 600;

	private JLabel lbID, lbTryAgain;
	private JLabel title;

	@Override
	public void initComponents() {
		setLayout(null);
		title = setTitle("");
		lbID = setTextLabel(COL_1, ROW_1, WDITH, 80, "Invalid");
		lbTryAgain = setTextLabel(COL_2, ROW_2, WDITH, 80, "Please try again!");

	}

	/**
	 * 
	 * @param isTicketBarCode: is barcode or not
	 */
	public void setInvalidInfo(Boolean isTicketBarCode) {
		if (isTicketBarCode) {
			lbID.setText("Can not find Ticket Matched");
			title.setText("Invalid Ticket");
		} else {
			lbID.setText("Invalid Barcode");
			title.setText("Invalid Barcode");
		}
	}

	/**
	 * 
	 * @param wrongDir state of AFC machine
	 */
	public void setInvalidDirection(AFCState wrongDir) {
		if (wrongDir == AFCState.WRONG_DIRECTION_NOT_IN) {
			title.setText("Can not Check In");
			lbID.setText("This Ticket is not used for Check In");
			lbTryAgain.setText("Just Check Out");
		}
		if (wrongDir == AFCState.WRONG_DIRECTION_NOT_OUT) {
			title.setText("Can not Check Out");
			lbID.setText("This Ticket is not used for Check Out");
			lbTryAgain.setText("Just Check In");
		}
	}

	@Override
	public void registerListener() {
		// TODO Auto-generated method stub

	}

	@Override
	public void addComponents() {
		// TODO Auto-generated method stub
		add(title);
		add(lbID);
		add(lbTryAgain);
		addBackButton();
	}
}
