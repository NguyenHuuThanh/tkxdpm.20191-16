package com.group16.afc.view;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import javax.swing.JLabel;

import com.group16.afc.database.Mysql;
import com.group16.afc.model.AFC;
import com.group16.afc.model.Station;

/**
 * 
 * @author nguyen huu thang
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.view
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description panel setup station for afc machine
 */
public class SetupStationPanel extends BasePanel {
	private static final int ROW_1 = 100;
	private static final int ROW_2 = 250;
	private static final int ROW_3 = 400;
	private static final int ROW_4 = 550;
	private static final int COL_1 = 50;
	private static final int COL_2 = 300;
	private static final int COL_3 = 550;

	private JLabel title;
	private JLabel lbCatLinh;
	private JLabel lbLaThanh;
	private JLabel lbThaiHa;
	private JLabel lbLangHa;
	private JLabel lbThuongDinh;
	private JLabel lbVanhDai3;
	private JLabel lbPhungKhoang;
	private JLabel lbVanQuan;
	private JLabel lbHaDong;
	private JLabel lbLaKhe;
	private JLabel lbVanKhe;
	private JLabel lbYenNghia;

	@Override
	public void initComponents() {
		setLayout(null);
		String str = "Choose Station";
		title = setTitle(str);
		lbCatLinh = setJLabel(COL_1, ROW_1, "Cat Linh");
		lbLaThanh = setJLabel(COL_2, ROW_1, "La Thanh");
		lbThaiHa = setJLabel(COL_3, ROW_1, "Thai Ha");
		lbLangHa = setJLabel(COL_1, ROW_2, "Lang Ha");
		lbThuongDinh = setJLabel(COL_2, ROW_2, "Thuong Dinh");
		lbVanhDai3 = setJLabel(COL_3, ROW_2, "Vanh Dai 3");
		lbPhungKhoang = setJLabel(COL_1, ROW_3, "Phung Khoang");
		lbVanQuan = setJLabel(COL_2, ROW_3, "Van Quan");
		lbHaDong = setJLabel(COL_3, ROW_3, "Ha Dong");
		lbLaKhe = setJLabel(COL_1, ROW_4, "La Khe");
		lbVanKhe = setJLabel(COL_2, ROW_4, "Van Khe");
		lbYenNghia = setJLabel(COL_3, ROW_4, "Yen Nghia");
	}

	@Override
	public void registerListener() {
		lbCatLinh.addMouseListener(mouseAdapter);
		lbLaThanh.addMouseListener(mouseAdapter);
		lbThaiHa.addMouseListener(mouseAdapter);
		lbLangHa.addMouseListener(mouseAdapter);
		lbThuongDinh.addMouseListener(mouseAdapter);
		lbVanhDai3.addMouseListener(mouseAdapter);
		lbPhungKhoang.addMouseListener(mouseAdapter);
		lbVanQuan.addMouseListener(mouseAdapter);
		lbHaDong.addMouseListener(mouseAdapter);
		lbLaKhe.addMouseListener(mouseAdapter);
		lbVanKhe.addMouseListener(mouseAdapter);
		lbYenNghia.addMouseListener(mouseAdapter);
	}

	@Override
	public void addComponents() {
		add(title);
		add(lbCatLinh);
		add(lbLaThanh);
		add(lbThaiHa);
		add(lbLangHa);
		add(lbThuongDinh);
		add(lbVanhDai3);
		add(lbPhungKhoang);
		add(lbVanQuan);
		add(lbHaDong);
		add(lbLaKhe);
		add(lbVanKhe);
		add(lbYenNghia);
	}

	@Override
	protected void clickedManipulation(MouseEvent e) {
		// TODO Auto-generated method stub
		AFCGUI.getInstance().afc = new AFC();
		if (e.getSource() == lbCatLinh) {
			AFCGUI.getInstance().afc.setStation(getStation(lbCatLinh));
		} else if (e.getSource() == lbLaThanh) {
			AFCGUI.getInstance().afc.setStation(getStation(lbLaThanh));
		} else if (e.getSource() == lbThaiHa) {
			AFCGUI.getInstance().afc.setStation(getStation(lbThaiHa));
		} else if (e.getSource() == lbLangHa) {
			AFCGUI.getInstance().afc.setStation(getStation(lbLangHa));
		} else if (e.getSource() == lbThuongDinh) {
			AFCGUI.getInstance().afc.setStation(getStation(lbThuongDinh));
		} else if (e.getSource() == lbVanhDai3) {
			AFCGUI.getInstance().afc.setStation(getStation(lbVanhDai3));
		} else if (e.getSource() == lbPhungKhoang) {
			AFCGUI.getInstance().afc.setStation(getStation(lbPhungKhoang));
		} else if (e.getSource() == lbVanQuan) {
			AFCGUI.getInstance().afc.setStation(getStation(lbVanQuan));
		} else if (e.getSource() == lbHaDong) {
			AFCGUI.getInstance().afc.setStation(getStation(lbHaDong));
		} else if (e.getSource() == lbLaKhe) {
			AFCGUI.getInstance().afc.setStation(getStation(lbLaKhe));
		} else if (e.getSource() == lbVanKhe) {
			AFCGUI.getInstance().afc.setStation(getStation(lbVanKhe));
		} else if (e.getSource() == lbYenNghia) {
			AFCGUI.getInstance().afc.setStation(getStation(lbYenNghia));
		}
		myContainer.showMethod();
	}

	public Station getStation(JLabel textLabel) {
		try {
			return Mysql.getInstance().getStation(textLabel.getText());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
	}
}
