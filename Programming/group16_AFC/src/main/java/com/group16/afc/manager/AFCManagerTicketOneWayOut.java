package com.group16.afc.manager;

import java.time.LocalDateTime;
import com.group16.afc.model.Station;
import com.group16.afc.model.TicketOneWay;
import com.group16.afc.model.TicketOneWayStatus;
import com.group16.afc.model.Travel;
import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.recognizer.TicketRecognizer;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.manager
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description Manager for OW OUT
 */
public class AFCManagerTicketOneWayOut extends AFCManager {

	public TicketOneWay ticketOW;

	@Override
	public boolean validateTicket() {
		try {
			code = TicketRecognizer.getInstance().process(barcode);
			if (code != null) {
				setState(AFCState.BARCODE_VALID);
				System.out.println(getState());
				return true;
			}

		} catch (InvalidIDException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setState(AFCState.BARCODE_INVALID);
			System.out.println(getState());
			return false;
		}
		setState(AFCState.BARCODE_INVALID);
		System.out.println(getState());
		return false;

	}

	@Override
	public boolean getInforTicket() {
		if (getState() == AFCState.BARCODE_VALID) {
			TicketOneWay t = getDatabase().getOW(code);
			System.out.println(t);
			if (t != null) {
				ticketOW = t;
				setState(AFCState.GET_INFO_SUCCESS);
				System.out.println(getState());
				return true;
			}
		}
		setState(AFCState.GET_INFO_FAIL);
		System.out.println(getState());
		return false;
	}

	@Override
	public boolean isAvailableTicket() {
		if (getState() == AFCState.GET_INFO_SUCCESS) {
			TicketOneWay t = ticketOW;
			if (t != null) {
				System.out.println(t);
				if (t.getStatus() == TicketOneWayStatus.NEW) {
					setState(AFCState.WRONG_DIRECTION_NOT_OUT);
					System.out.println(getState());
					return false;
				}
				if (t.getStatus().compareTo(TicketOneWayStatus.USING) == 0) {
					Travel travel = getDatabase().getTravel(t.getId());
					t.setNewestTravel(travel);
					Station inStation = getDatabase().getStation(travel.getInStation().getId());
					Station outStation = getDatabase().getStation(afc.getStation().getId());
					double cost = getCaculateBalance().calculateBalance(inStation, outStation);
					t.getNewestTravel().setBalance(cost);
					t.getNewestTravel().setInStation(inStation);
					t.getNewestTravel().setOutStation(outStation);
					double cash = getCaculateBalance().calculateBalance(t.getInStation(), t.getOutStation());
					if (cash >= cost) {
						setState(AFCState.AVAILBLE_TICKET);
						t.setStatus(TicketOneWayStatus.USED);
						t.getNewestTravel().setExpTime(LocalDateTime.now());
						System.out.println(getState());
						return true;
					} else {
						setState(AFCState.NOT_ENOUGH_BALANCE);
						System.out.println(getState());
						return false;
					}
				}
				if (t.getStatus() == TicketOneWayStatus.USED) {
					setState(AFCState.EXPRIED_TICKET);
					System.out.println(getState());
					return false;
				}

			}
		}
		setState(AFCState.UNAVAILBLE_TICKET);
		System.out.println(getState());
		return false;
	}

	@Override
	public boolean updateInfoTicket() {
		if (getState() == AFCState.AVAILBLE_TICKET) {
			TicketOneWay t = ticketOW;
			try {
				getDatabase().updateOWStatus(t.getId(), t.getStatus().toString());
				getDatabase().updateTravelOutPoint(t.getNewestTravel().getOutStation().getId(),
						t.getNewestTravel().getId());
				getDatabase().updateTravelEndTime(t.getNewestTravel().getExpTime(), t.getNewestTravel().getId());
				setState(AFCState.UPDATE_SUCCESS);
				System.out.println(getState());
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				setState(AFCState.UPDATE_FAIL);
				System.out.println(getState());
				return false;
			}

		}
		setState(AFCState.UPDATE_FAIL);
		System.out.println(getState());
		return false;
	}

	@Override
	public boolean controlGate() {
		if (getState() == AFCState.UPDATE_SUCCESS) {
			GateManager.process();
			setState(AFCState.DONE);
			System.out.println(getState());
		}
		return false;
	}

	@Override
	public void processTicket() {
		validateTicket();
		getInforTicket();
		isAvailableTicket();
		updateInfoTicket();
		controlGate();
	}

}
