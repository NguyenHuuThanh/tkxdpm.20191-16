package com.group16.afc.model;

import java.time.LocalDateTime;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.model
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description store data of Travel
 */
public class Travel {
	/**
	 * id
	 */
	protected int id;
	/**
	 * {@link Station} you go in
	 */
	protected Station inStation;
	/**
	 * {@link Station} you go out
	 */
	protected Station outStation;
	/**
	 * cost of travel
	 */
	protected double balance;
	/**
	 * start Time when check in
	 */
	protected LocalDateTime startTime;
	/**
	 * end time when check out
	 */
	protected LocalDateTime endTime;

	public Travel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Station getInStation() {
		return inStation;
	}

	public void setInStation(Station inStation) {
		this.inStation = inStation;
	}

	public Station getOutStation() {
		return outStation;
	}

	public void setOutStation(Station outStation) {
		this.outStation = outStation;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getExpTime() {
		return endTime;
	}

	public void setExpTime(LocalDateTime expTime) {
		this.endTime = expTime;
	}

	@Override
	public String toString() {
		return "Travel [id=" + id + ", inStation=" + inStation + ", outStation=" + outStation + ", balance=" + balance
				+ ", startTime=" + startTime + ", endTime=" + endTime + "]";
	}
}
