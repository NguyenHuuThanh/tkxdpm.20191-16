package com.group16.afc.manager;

import hust.soict.se.gate.Gate;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.manager
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description
 * Gate Controller
 * <p>
 * auto open and close gate
 */
public class GateManager {
	/**
	 * make new {@link Thread} for {@link Gate}
	 */
	public static void process() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				autoGate();
			}
		}).start();
	}

	/**
	 * auto control open and close
	 */
	public static void autoGate() {
		Gate.getInstance().open();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Gate.getInstance().close();
	}
}
