package com.group16.afc.model;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.model
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description list status of PC
 */
public enum PrepaidCardStatus {
	NEW, NEW_TRAVEL, END_TRAVEL, BLOCKED
}
