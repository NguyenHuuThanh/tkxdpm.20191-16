package com.group16.afc.view;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;

/**
 * 
 * @author nguyen huu thang
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.view
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description base panel for all panel
 */
public abstract class BasePanel extends JPanel implements Setup {
	public static final int WIDTH = 200;
	public static final int HEIGH = 80;
	public static final int FONT_SIZE = 20;
	public JButton home, welcome;
	public MyContainer myContainer;

	public MouseAdapter mouseAdapter = new MouseAdapter() {
		@Override
		public void mouseEntered(MouseEvent e) {
			enteredManipulation(e);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			exitedManipulation(e);
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			clickedManipulation(e);
		}
	};

	public BasePanel() {
		home = setJButton(100, 550, "Setup");
		welcome = setJButton(500, 550, "Welcome");
		home.addMouseListener(mouseAdapter);
		welcome.addMouseListener(mouseAdapter);

		initComponents();
		registerListener();
		addComponents();
	}

	protected void enteredManipulation(MouseEvent e) {

	}

	protected void exitedManipulation(MouseEvent e) {

	}

	protected void clickedManipulation(MouseEvent e) {
		if (myContainer == null) return;
		if (e.getSource() == home) {
			myContainer.showSetup();
		} else if (e.getSource() == welcome) {
			myContainer.showWelcome();
		}
	}

	public void setMycontainer(MyContainer container) {
		this.myContainer = container;
	}

	public void addBackButton() {
		add(home);
		add(welcome);
	}

	/**
	 * 
	 * @param text: text in title
	 * @return JLabel: text label use for title
	 */
	public JLabel setTitle(String text) {
		JLabel jLabel = new JLabel();
		ImageIcon imageIcon = new ImageIcon(
				ImageLoader.BAR.getScaledInstance(AFCGUI.WIDTH_FRAME, 80, java.awt.Image.SCALE_SMOOTH));
		jLabel.setBounds(0, 0, imageIcon.getIconWidth(), imageIcon.getIconHeight());
		jLabel.setIcon(imageIcon);
		jLabel.setText(text);
		jLabel.setHorizontalTextPosition(JLabel.CENTER);
		jLabel.setVerticalTextPosition(JLabel.CENTER);
		jLabel.setFont(new Font(jLabel.getFont().getFontName(), Font.PLAIN, FONT_SIZE + 5));
		return jLabel;
	}

	/**
	 * 
	 * @param x:    x position of label
	 * @param y:    y position of label
	 * @param text: text inside label
	 * @return JLabel - a label has text in center
	 */
	public JLabel setJLabel(int x, int y, String text) {
		JLabel jLabel = new JLabel();
		ImageIcon imageIcon = new ImageIcon(
				ImageLoader.BAR.getScaledInstance(WIDTH, HEIGH, java.awt.Image.SCALE_SMOOTH));
		jLabel.setBounds(x, y, imageIcon.getIconWidth(), imageIcon.getIconHeight());
		jLabel.setIcon(imageIcon);
		jLabel.setText(text);
		jLabel.setHorizontalTextPosition(JLabel.CENTER);
		jLabel.setVerticalTextPosition(JLabel.CENTER);
		jLabel.setFont(new Font(jLabel.getFont().getFontName(), Font.PLAIN, FONT_SIZE));
		return jLabel;
	}

	/**
	 * 
	 * @param x:      x position of label
	 * @param y:      y position of label
	 * @param witdh:  label width
	 * @param height: label height
	 * @param text:   text inside label
	 * @return JLabel - a label has text in center
	 */
	public JLabel setTextLabel(int x, int y, int witdh, int height, String text) {
		JLabel jLabel = new JLabel();
		jLabel.setBounds(x, y, witdh, height);
		jLabel.setText(text);
		jLabel.setHorizontalTextPosition(JLabel.CENTER);
		jLabel.setVerticalTextPosition(JLabel.CENTER);
		jLabel.setFont(new Font(jLabel.getFont().getFontName(), Font.PLAIN, FONT_SIZE));
		return jLabel;
	}

	/**
	 * 
	 * @param x:    x position of button
	 * @param y:    y position of button
	 * @param text: button name
	 * @return JButton - buttn with name
	 */
	public JButton setJButton(int x, int y, String text) {
		JButton jButton = new JButton();
		ImageIcon imageIcon = new ImageIcon(
				ImageLoader.BAR.getScaledInstance(WIDTH, HEIGH, java.awt.Image.SCALE_SMOOTH));
		jButton.setBounds(x, y, imageIcon.getIconWidth(), imageIcon.getIconHeight());
		jButton.setIcon(imageIcon);
		jButton.setText(text);
		jButton.setHorizontalTextPosition(JLabel.CENTER);
		jButton.setVerticalTextPosition(JLabel.CENTER);
		jButton.setFont(new Font(jButton.getFont().getFontName(), Font.PLAIN, FONT_SIZE));
		return jButton;
	}

	/**
	 * 
	 * @param x:      x position
	 * @param y:      y position
	 * @param witdh:  textarea width
	 * @param height: textarea height
	 * @param text:   text of text area
	 * @return JTextArea
	 */
	public JTextArea setJTextArea(int x, int y, int witdh, int height, String text) {
		JTextArea jText = new JTextArea();
		jText.setBounds(x, y, witdh, height);
		jText.setText(text);
		jText.setFont(new Font(jText.getFont().getFontName(), Font.PLAIN, FONT_SIZE));
		return jText;
	}

	/**
	 * 
	 * @param x:      x position
	 * @param y:      y position
	 * @param witdh:  text field width
	 * @param height: text field height
	 * @param text:   text in jtextfield
	 * @return JTextField
	 */
	public JTextField setJTextField(int x, int y, int witdh, int height, String text) {
		JTextField jText = new JTextField();
		jText.setBounds(x, y, witdh, height);
		jText.setText(text);
		jText.setFont(new Font(jText.getFont().getFontName(), Font.PLAIN, FONT_SIZE));
		return jText;
	}
}
