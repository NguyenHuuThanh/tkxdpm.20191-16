package com.group16.afc.model;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.model
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description save data of AFC
 */
public class AFC {
	/**
	 * id of AFC
	 */
	private int id;
	/**
	 * {@link Station} of AFC
	 */
	private Station station;
	/**
	 * {@link Direction} of AFC
	 */
	private Direction direction;

	public AFC() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AFC(int id, Station station, Direction direction) {
		super();
		this.id = id;
		this.station = station;
		this.direction = direction;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Station getStation() {
		return station;
	}

	public void setStation(Station station) {
		this.station = station;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

}
