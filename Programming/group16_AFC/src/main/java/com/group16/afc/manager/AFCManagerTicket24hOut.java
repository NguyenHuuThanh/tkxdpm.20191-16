package com.group16.afc.manager;

import java.time.LocalDateTime;
import com.group16.afc.model.Ticket24h;
import com.group16.afc.model.Ticket24hStatus;
import com.group16.afc.model.Travel;
import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.recognizer.TicketRecognizer;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.manager
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description Manager for TF OUT
 */
public class AFCManagerTicket24hOut extends AFCManager {

	public Ticket24h ticket24h;

	@Override
	public boolean validateTicket() {
		try {
			code = TicketRecognizer.getInstance().process(barcode);
			if (code != null) {
				setState(AFCState.BARCODE_VALID);
				System.out.println(getState());
				return true;
			}

		} catch (InvalidIDException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setState(AFCState.BARCODE_INVALID);
			System.out.println(getState());
			return false;
		}
		setState(AFCState.BARCODE_INVALID);
		System.out.println(getState());
		return false;
	}

	@Override
	public boolean getInforTicket() {
		if (getState() == AFCState.BARCODE_VALID) {
			Ticket24h t = getDatabase().getTF(code);
			System.out.println(t);
			if (t != null) {
				ticket24h = t;
				setState(AFCState.GET_INFO_SUCCESS);
				System.out.println(getState());
				return true;
			}
		}
		setState(AFCState.GET_INFO_FAIL);
		System.out.println(getState());
		return false;
	}

	@Override
	public boolean isAvailableTicket() {
		if (getState() == AFCState.GET_INFO_SUCCESS) {
			Ticket24h t = ticket24h;
			if (t != null) {
				System.out.println(t);
				LocalDateTime time = LocalDateTime.now();
				if (t.getStatus() == Ticket24hStatus.NEW || t.getStatus() == Ticket24hStatus.END_TRAVEL) {
					setState(AFCState.WRONG_DIRECTION_NOT_OUT);
					System.out.println(getState());
					return false;
				} else {
					if (t.getStatus() == Ticket24hStatus.NEW_TRAVEL) {
						if (time.isBefore(t.getExpTime().plusSeconds(1))) {
							ticket24h.setStatus(Ticket24hStatus.END_TRAVEL);
							setState(AFCState.AVAILBLE_TICKET);
							System.out.println(getState());
							return true;
						}
						// exp ticket
						else {
							System.out.println("????");
							ticket24h.setStatus(Ticket24hStatus.EXPIRED);
							setState(AFCState.AVAILBLE_TICKET);
							System.out.println(getState());
							return true;
						}
					} else if (t.getStatus() == Ticket24hStatus.EXPIRED) {
						setState(AFCState.EXPRIED_TICKET);
						System.out.println(getState());
						return false;
					}

				}

			}
		}
		setState(AFCState.UNAVAILBLE_TICKET);
		System.out.println(getState());
		return false;
	}

	@Override
	public boolean updateInfoTicket() {
		if (getState() == AFCState.AVAILBLE_TICKET) {
			Ticket24h t = ticket24h;

			// using ticket
			Travel travel = getDatabase().getTravel(ticket24h.getId());
			if (afc != null) getDatabase().updateTravelOutPoint(afc.getStation().getId(), travel.getId());
			getDatabase().updateTravelEndTime(LocalDateTime.now(), travel.getId());
			getDatabase().updateTFStatus(t.getId(), ticket24h.getStatus().toString());
			setState(AFCState.UPDATE_SUCCESS);
			System.out.println(getState());
			return true;
		} else if (ticket24h.getStatus() == Ticket24hStatus.EXPIRED) {
			getDatabase().updateTFStatus(ticket24h.getId(), ticket24h.getStatus().toString());
			System.out.println(getState());
			return true;
		}
		setState(AFCState.UPDATE_FAIL);
		System.out.println(getState());
		return false;
	}

	@Override
	public boolean controlGate() {
		if (getState() == AFCState.UPDATE_SUCCESS) {
			GateManager.process();
			setState(AFCState.DONE);
			System.out.println(getState());
		}
		return false;
	}

	@Override
	public void processTicket() {
		validateTicket();
		getInforTicket();
		isAvailableTicket();
		updateInfoTicket();
		controlGate();
	}

}
