package com.group16.afc.database;

import java.time.LocalDateTime;

import com.group16.afc.model.Ticket24h;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.database
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description Ticket TF DAO
 */
public interface DatabaseTF {
	/**
	 * get TF by code
	 * 
	 * @param _code {@link String}
	 * @return {@link Ticket24h}
	 */
	public Ticket24h getTF(String _code);

	/**
	 * update TF Time for setting beginning of period that {@link Ticket24h}
	 * available
	 * 
	 * @param ticketId {@link String}
	 * @param startTime {@link LocalDateTime}
	 * @return <code>true</code> if complete
	 */
	public boolean updateTFTime(String ticketId, LocalDateTime startTime);

	/**
	 * update status of TF
	 * 
	 * @param ticketId {@link String}
	 * @param status {@link String}
	 * @return <code>true</code> if complete
	 */
	public boolean updateTFStatus(String ticketId, String status);
}
