package com.group16.afc.model;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.model
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description store date of OW
 */
public class TicketOneWay extends Ticket {
	/**
	 * status
	 */
	protected TicketOneWayStatus status;
	/**
	 * travel
	 */
	protected Travel travel;
	/**
	 * start Station writen in OW
	 */
	protected Station inStation;
	/**
	 * end Station writen in OW
	 */
	protected Station outStation;

	public TicketOneWay() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TicketOneWayStatus getStatus() {
		return status;
	}

	public void setStatus(TicketOneWayStatus status) {
		this.status = status;
	}

	public Station getInStation() {
		return inStation;
	}

	public void setInStation(Station inStation) {
		this.inStation = inStation;
	}

	public Station getOutStation() {
		return outStation;
	}

	public void setOutStation(Station outStation) {
		this.outStation = outStation;
	}

	@Override
	public String toString() {
		return "TicketOneWay [status=" + status + ", travel=" + travel + ", inStation=" + inStation + ", outStation="
				+ outStation + ", id=" + id + ", timeBuy=" + timeBuy + ", code=" + code + "]";
	}

	@Override
	public boolean isOneWay() {
		// TODO Auto-generated method stub
		return true;
	}

}
