package com.group16.afc.database;

import com.group16.afc.model.Station;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.database
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description Station DAO
 */
public interface DatabaseStation {
	/**
	 * get {@link Station} by name
	 * 
	 * @param name {@link String}
	 * @return {@link Station}
	 */
	public Station getStation(String name);

	/**
	 * get {@link Station} by id
	 * 
	 * @param _id int
	 * @return {@link Station}
	 */
	public Station getStation(int _id);
}
