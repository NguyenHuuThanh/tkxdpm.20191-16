package com.group16.afc.database;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.database
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description interface define instance would be concreted
 */
public interface Database extends DatabaseOW, DatabasePC, DatabaseTF, DatabaseTravel, DatabaseStation {

}
