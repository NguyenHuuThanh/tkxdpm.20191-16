package com.group16.afc.view;

import javax.swing.JLabel;

import com.group16.afc.manager.AFCState;
import com.group16.afc.model.Direction;
import com.group16.afc.model.PrepaidCard;

/**
 * 
 * @author nguyen huu thang
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.view
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description panel for PC
 */
public class CardPanel extends BasePanel {
	private static final int ROW_1 = 80;
	private static final int ROW_2 = 160;
	private static final int ROW_3 = 240;
	private static final int ROW_4 = 320;
	private static final int ROW_5 = 400;
	private static final int ROW_6 = 480;
	private static final int COL_1 = 50;
	private static final int COL_2 = 300;
	private static final int WDITH = 600;

	private JLabel lbID, lbCash, lbInPoint, lbOutpoint, lbBalance, lbTranfers;
	private JLabel title;
	private JLabel lbIDValue, lbCashValue, lbInPointValue, lbOutpointValue, lbBalanceValue;

	@Override
	public void initComponents() {
		setLayout(null);
		title = setTitle("");
		lbID = setTextLabel(COL_1, ROW_1, WDITH, 80, "ID");
		lbCash = setTextLabel(COL_1, ROW_2, WDITH, 80, "Cash");
		lbInPoint = setTextLabel(COL_1, ROW_3, WDITH, 80, "Enter Station");
		lbOutpoint = setTextLabel(COL_1, ROW_4, WDITH, 80, "Exit Station");
		lbBalance = setTextLabel(COL_1, ROW_5, WDITH, 80, "Balance");
		lbIDValue = setTextLabel(COL_2, ROW_1, WDITH, 80, "1");
		lbCashValue = setTextLabel(COL_2, ROW_2, WDITH, 80, "1");
		lbInPointValue = setTextLabel(COL_2, ROW_3, WDITH, 80, "1");
		lbOutpointValue = setTextLabel(COL_2, ROW_4, WDITH, 80, "1");
		lbBalanceValue = setTextLabel(COL_2, ROW_5, WDITH, 80, "1");
		lbTranfers = setTextLabel(COL_1, ROW_6, WDITH, 80, "Please tranfers money to your card!");
		lbTranfers.setVisible(false);
	}

	/**
	 * 
	 * @param afcstate afc machine state
	 * @param card     PrepaidCard object use for display on GUI
	 */
	public void setCardInfo(AFCState afcstate, PrepaidCard card) {
		if (afcstate == AFCState.EXPRIED_TICKET) {
			title.setText("Card is blocked");
		} else if (afcstate == AFCState.NOT_ENOUGH_BASE_BALANCE) {
			title.setText("Not enough base balance");

		} else if (afcstate == AFCState.NOT_ENOUGH_BALANCE) {
			title.setText("Not enough balance");
			lbTranfers.setVisible(true);
		} else if (afcstate == AFCState.AVAILBLE_TICKET) {
			title.setText("Opening gate with Prepaid Card Ticket");
			lbTranfers.setVisible(false);
		}

		visibleField(true);
		lbIDValue.setText(card.getId());
		lbCashValue.setText(card.getCash() + " VND");

		if (afcstate == AFCState.NOT_ENOUGH_BASE_BALANCE) {
			lbOutpoint.setVisible(false);
			lbBalanceValue.setVisible(false);
			lbBalance.setVisible(false);
			lbOutpointValue.setVisible(false);
			lbInPoint.setVisible(false);
			lbInPointValue.setText("Please charge money");
		}

		if (afcstate != AFCState.NOT_ENOUGH_BASE_BALANCE) {
			lbInPointValue.setText(card.getNewestTravel().getInStation().getName());

			if (AFCGUI.getInstance().afc.getDirection() == Direction.OUT) {
				if (card.getNewestTravel().getOutStation() != null)
					lbOutpointValue.setText(card.getNewestTravel().getOutStation().getName());
				lbBalanceValue.setText(card.getNewestTravel().getBalance() + " VND");
			} else {
				lbOutpoint.setVisible(false);
				lbBalanceValue.setVisible(false);
				lbBalance.setVisible(false);
				lbOutpointValue.setVisible(false);
			}
		}

	}

	private void visibleField(boolean visible) {
		lbOutpoint.setVisible(visible);
		lbBalanceValue.setVisible(visible);
		lbBalance.setVisible(visible);
		lbOutpointValue.setVisible(visible);
	}

	@Override
	public void registerListener() {
		// TODO Auto-generated method stub

	}

	@Override
	public void addComponents() {
		// TODO Auto-generated method stub
		add(title);
		add(lbID);
		add(lbCash);
		add(lbInPoint);
		add(lbOutpoint);
		add(lbBalance);
		add(lbIDValue);
		add(lbCashValue);
		add(lbInPointValue);
		add(lbOutpointValue);
		add(lbBalanceValue);
		add(lbTranfers);
		addBackButton();
	}

}
