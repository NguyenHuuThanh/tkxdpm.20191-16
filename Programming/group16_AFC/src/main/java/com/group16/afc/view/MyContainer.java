package com.group16.afc.view;

import java.awt.CardLayout;

import com.group16.afc.manager.AFCState;
import com.group16.afc.model.PrepaidCard;
import com.group16.afc.model.Ticket24h;
import com.group16.afc.model.TicketOneWay;

/**
 * 
 * @author nguyen huu thang
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.view
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description manage all screen
 */
public class MyContainer extends BasePanel {
	private static final String TAG_METHOD = "tag_method";
	private static final String TAG_SETUP = "tag_setup";
	private static final String TAG_WELCOME = "tag_welcome";
	private static final String TAG_ONEWAY = "tag_oneway";
	private static final String TAG_ONEDAY = "tag_oneday";
	private static final String TAG_CARD = "tag_card";
	private static final String TAG_INVALID = "tag_invalid";

	private SetupStationPanel setupPanel;
	private SetupDirectionPanel setupMethodPanel;
	private WelcomePanel welcomPanel;
	private OneWayPanel oneWayPanel;
	private OneDayPanel oneDayPanel;
	private CardPanel cardPanel;
	private InvalidPanel invalidPanel;

	private CardLayout cardLayout;
	private AFCGUI gui;

	@Override
	public void initComponents() {
		cardLayout = new CardLayout();
		setLayout(cardLayout);

		setupPanel = new SetupStationPanel();
		setupPanel.setMycontainer(this);

		setupMethodPanel = new SetupDirectionPanel();
		setupMethodPanel.setMycontainer(this);

		welcomPanel = new WelcomePanel();
		welcomPanel.setMycontainer(this);

		oneWayPanel = new OneWayPanel();
		oneWayPanel.setMycontainer(this);

		oneDayPanel = new OneDayPanel();
		oneDayPanel.setMycontainer(this);

		cardPanel = new CardPanel();
		cardPanel.setMycontainer(this);

		invalidPanel = new InvalidPanel();
		invalidPanel.setMycontainer(this);
	}

	@Override
	public void registerListener() {
		// TODO Auto-generated method stub

	}

	@Override
	public void addComponents() {
		add(setupPanel, TAG_SETUP);
		add(setupMethodPanel, TAG_METHOD);
		add(welcomPanel, TAG_WELCOME);
		add(oneWayPanel, TAG_ONEWAY);
		add(oneDayPanel, TAG_ONEDAY);
		add(cardPanel, TAG_CARD);
		add(invalidPanel, TAG_INVALID);
	}

	public void setGui(AFCGUI gui) {
		this.gui = gui;
	}

	public void showSetup() {
		cardLayout.show(this, TAG_SETUP);
		setupPanel.requestFocus();
	}

	public void showMethod() {
		cardLayout.show(this, TAG_METHOD);
		setupMethodPanel.requestFocus();
	}

	public void showWelcome() {
		cardLayout.show(this, TAG_WELCOME);
		welcomPanel.requestFocus();
	}

	public void showOneWay(AFCState afcstate, TicketOneWay ticket) {
		cardLayout.show(this, TAG_ONEWAY);
		oneWayPanel.setOneWayInfo(afcstate, ticket);
		oneWayPanel.requestFocus();
	}

	public void showOneDay(AFCState afcstate, Ticket24h ticket) {
		cardLayout.show(this, TAG_ONEDAY);
		oneDayPanel.setOneDayInfo(afcstate, ticket);
		oneDayPanel.requestFocus();
	}

	public void showCard(AFCState afcstate, PrepaidCard card) {
		cardLayout.show(this, TAG_CARD);
		cardPanel.setCardInfo(afcstate, card);
		cardPanel.requestFocus();
	}

	public void showInvalid(boolean isTicketBarcode) {
		cardLayout.show(this, TAG_INVALID);
		invalidPanel.setInvalidInfo(isTicketBarcode);
		invalidPanel.requestFocus();
	}

	public void showWrongDirection(AFCState wrongDir) {
		cardLayout.show(this, TAG_INVALID);
		invalidPanel.setInvalidDirection(wrongDir);
		invalidPanel.requestFocus();
	}
}
