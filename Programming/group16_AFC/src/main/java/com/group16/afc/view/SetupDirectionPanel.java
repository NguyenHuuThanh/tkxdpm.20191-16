package com.group16.afc.view;

import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import com.group16.afc.model.Direction;

/**
 * 
 * @author nguyen huu thang
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.view
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description set up afc direction
 */
public class SetupDirectionPanel extends BasePanel {
	private static final int COL_1 = 100;
	private static final int COL_2 = 500;
	private static final int ROW_1 = 300;

	private JLabel title;
	private JLabel lbIn;
	private JLabel lbOut;

	@Override
	public void initComponents() {
		setLayout(null);
		String str = "Choose Direction";
		lbIn = setJLabel(COL_1, ROW_1, "Check In");
		lbOut = setJLabel(COL_2, ROW_1, "Check Out");
		title = setTitle(str);
	}

	@Override
	public void registerListener() {
		lbIn.addMouseListener(mouseAdapter);
		lbOut.addMouseListener(mouseAdapter);
	}

	@Override
	protected void clickedManipulation(MouseEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == lbIn) {
			AFCGUI.getInstance().afc.setDirection(Direction.IN);
		} else if (e.getSource() == lbOut) {
			AFCGUI.getInstance().afc.setDirection(Direction.OUT);
		}
		myContainer.showWelcome();
	}

	@Override
	public void addComponents() {
		add(title);
		add(lbIn);
		add(lbOut);
	}
}
