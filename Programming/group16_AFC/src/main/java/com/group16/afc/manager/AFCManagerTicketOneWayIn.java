package com.group16.afc.manager;

import java.time.LocalDateTime;
import com.group16.afc.model.Station;
import com.group16.afc.model.TicketOneWay;
import com.group16.afc.model.TicketOneWayStatus;
import com.group16.afc.model.Travel;
import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.recognizer.TicketRecognizer;

/**
 * 
 * @author Nguyen Van Thang
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.manager
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description Manager for OW IN
 */
public class AFCManagerTicketOneWayIn extends AFCManager {

	boolean numberInRange(double range1, double range2, double a) {
		if (range1 <= a && a <= range2) return true;
		if (range2 <= a && a <= range1) return true;
		return false;
	}

	public TicketOneWay ticketOW;

	@Override
	public boolean validateTicket() {
		try {
			code = TicketRecognizer.getInstance().process(barcode);
			if (code != null) {
				setState(AFCState.BARCODE_VALID);
				System.out.println(getState());
				return true;
			}

		} catch (InvalidIDException e) {
			e.printStackTrace();
			setState(AFCState.BARCODE_INVALID);
			System.out.println(getState());
			return false;
		}
		setState(AFCState.BARCODE_INVALID);
		System.out.println(getState());
		return false;

	}

	@Override
	public boolean getInforTicket() {
		if (getState() == AFCState.BARCODE_VALID) {
			TicketOneWay t = getDatabase().getOW(code);
			System.out.println(t);
			if (t != null) {
				ticketOW = t;
				setState(AFCState.GET_INFO_SUCCESS);
				System.out.println(getState());
				return true;
			}
		}
		setState(AFCState.GET_INFO_FAIL);
		System.out.println(getState());
		return false;
	}

	@Override
	public boolean isAvailableTicket() {
		if (getState() == AFCState.GET_INFO_SUCCESS) {
			TicketOneWay t = ticketOW;
			if (t != null) {
				if (afc != null) {
					Travel travel = new Travel();
					if (afc != null) travel.setInStation(getDatabase().getStation(afc.getStation().getId()));
					t.setNewestTravel(travel);
				}
				System.out.println(t);
				if (t.getStatus().compareTo(TicketOneWayStatus.NEW) == 0) {
					Station startRangeStation = getDatabase().getStation(t.getInStation().getId());
					Station endRangeStation = getDatabase().getStation(t.getOutStation().getId());
					Station nowStation = getDatabase().getStation(t.getNewestTravel().getInStation().getId());
					if (numberInRange(startRangeStation.getDistance(), endRangeStation.getDistance(),
							nowStation.getDistance())) {
						t.getNewestTravel().setStartTime(LocalDateTime.now());
						t.setStatus(TicketOneWayStatus.USING);
						setState(AFCState.AVAILBLE_TICKET);
						System.out.println(getState());
						return true;
					} else {
						setState(AFCState.WRONG_STATION);
						System.out.println(getState());
						return false;
					}

				} else if (t.getStatus() == TicketOneWayStatus.USING) {
					setState(AFCState.WRONG_DIRECTION_NOT_IN);
					System.out.println(getState());
					return false;
				} else if (t.getStatus() == TicketOneWayStatus.USED) {
					setState(AFCState.EXPRIED_TICKET);
					System.out.println(getState());
					return false;
				}

			}
		}
		setState(AFCState.UNAVAILBLE_TICKET);
		System.out.println(getState());
		return false;

	}

	@Override
	public boolean updateInfoTicket() {
		if (getState() == AFCState.AVAILBLE_TICKET) {
			TicketOneWay t = ticketOW;
			try {
				getDatabase().updateOWStatus(t.getId(), t.getStatus().toString());
				getDatabase().insertNewTravel(t.getId(), t.getNewestTravel().getInStation().getId(),
						t.getNewestTravel().getStartTime());
				setState(AFCState.UPDATE_SUCCESS);
				System.out.println(getState());
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				setState(AFCState.UPDATE_FAIL);
				System.out.println(getState());
				return false;
			}

		}
		setState(AFCState.UPDATE_FAIL);
		System.out.println(getState());
		return false;
	}

	@Override
	public boolean controlGate() {
		if (getState() == AFCState.UPDATE_SUCCESS) {
			GateManager.process();
			setState(AFCState.DONE);
			System.out.println(getState());
		}
		return false;
	}

	@Override
	public void processTicket() {
		validateTicket();
		getInforTicket();
		isAvailableTicket();
		updateInfoTicket();
		controlGate();
	}
}
