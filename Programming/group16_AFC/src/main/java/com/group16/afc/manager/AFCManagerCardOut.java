package com.group16.afc.manager;

import java.time.LocalDateTime;
import com.group16.afc.model.PrepaidCard;
import com.group16.afc.model.PrepaidCardStatus;
import com.group16.afc.model.Station;
import com.group16.afc.model.Travel;
import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.scanner.CardScanner;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.manager
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description Manager for PC OUT
 */
public class AFCManagerCardOut extends AFCManager {

	public PrepaidCard prepaidCard;

	@Override
	public boolean validateTicket() {
		try {
			code = CardScanner.getInstance().process(barcode);
			if (code != null) {
				setState(AFCState.BARCODE_VALID);
				System.out.println(getState());
				return true;
			}

		} catch (InvalidIDException e) {
			e.printStackTrace();
			setState(AFCState.BARCODE_INVALID);
			System.out.println(getState());
			return false;
		}
		setState(AFCState.BARCODE_INVALID);
		System.out.println(getState());
		return false;

	}

	@Override
	public boolean getInforTicket() {
		if (getState() == AFCState.BARCODE_VALID) {
			PrepaidCard t = getDatabase().getPC(code);
			prepaidCard = t;
			System.out.println(t);
			if (t != null) {
				ticket = t;
				setState(AFCState.GET_INFO_SUCCESS);
				System.out.println(getState());
				return true;
			}
		}
		setState(AFCState.GET_INFO_FAIL);
		System.out.println(getState());
		return false;
	}

	@Override
	public boolean isAvailableTicket() {
		if (getState() == AFCState.GET_INFO_SUCCESS) {
			PrepaidCard t = prepaidCard;
			if (t != null) {
				System.out.println(t);
				if (t.getStatus() == PrepaidCardStatus.NEW || t.getStatus() == PrepaidCardStatus.END_TRAVEL) {
					setState(AFCState.WRONG_DIRECTION_NOT_OUT);
					System.out.println(getState());
					return false;
				}

				if (t.getStatus().compareTo(PrepaidCardStatus.NEW_TRAVEL) == 0) {
					Travel travel = getDatabase().getTravel(t.getId());
					t.setNewestTravel(travel);
					Station inStation = getDatabase().getStation(travel.getInStation().getId());
					Station outStation = getDatabase().getStation(afc.getStation().getId());
					double cost = getCaculateBalance().calculateBalance(inStation, outStation);
					t.getNewestTravel().setBalance(cost);
					t.getNewestTravel().setInStation(inStation);
					t.getNewestTravel().setOutStation(outStation);
					if (t.getCash() >= cost) {
						double newCash = t.getCash() - cost;
						setState(AFCState.AVAILBLE_TICKET);
						t.getNewestTravel().setExpTime(LocalDateTime.now());
						t.setCash(newCash);
						t.setStatus(PrepaidCardStatus.END_TRAVEL);
						System.out.println(getState());
						return true;
					} else {
						setState(AFCState.NOT_ENOUGH_BALANCE);
						System.out.println(getState());
						return false;
					}
				}

				if (t.getStatus() == PrepaidCardStatus.BLOCKED) {
					setState(AFCState.EXPRIED_TICKET);
					System.out.println(getState());
					return false;
				}
			}
		}
		setState(AFCState.UNAVAILBLE_TICKET);
		System.out.println(getState());
		return false;

	}

	@Override
	public boolean updateInfoTicket() {
		if (getState() == AFCState.AVAILBLE_TICKET) {
			PrepaidCard t = prepaidCard;
			try {
				getDatabase().updatePCStatus(t.getId(), PrepaidCardStatus.END_TRAVEL.toString());
				getDatabase().updatePCCash(t.getId(), t.getCash());
				getDatabase().updateTravelOutPoint(afc.getStation().getId(), t.getNewestTravel().getId());
				getDatabase().updateTravelEndTime(t.getNewestTravel().getExpTime(), t.getNewestTravel().getId());
				setState(AFCState.UPDATE_SUCCESS);
				System.out.println(getState());
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				setState(AFCState.UPDATE_FAIL);
				System.out.println(getState());
				return false;
			}

		}
		setState(AFCState.UPDATE_FAIL);
		System.out.println(getState());
		return false;
	}

	@Override
	public boolean controlGate() {
		if (getState() == AFCState.UPDATE_SUCCESS) {
			GateManager.process();
			setState(AFCState.DONE);
			System.out.println(getState());
		}
		return false;
	}

	@Override
	public void processTicket() {
		validateTicket();
		getInforTicket();
		isAvailableTicket();
		updateInfoTicket();
		controlGate();
	}
}
