package com.group16.afc.app;

import java.sql.SQLException;

import com.group16.afc.database.Mysql;
import com.group16.afc.view.AFCGUI;

import config.Configuration;
import utils.CalculatingBalanceVND;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.app
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description 
 * 
 * Class run program.
 * <p>
 * Where set config and Run
 */
public class Main {
	/**
	 * description beginning of program
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Configuration.getInstance().setCalBalance(CalculatingBalanceVND.getInstance());
		try {
			Configuration.getInstance().setDb(Mysql.getInstance());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		AFCGUI gui = AFCGUI.getInstance();
		gui.setVisible(true);
	}
}
/*
 * TF Ticket 
 * aaaaaaaa, 3dbe00a167653a1a 
 * bbbbbbbb, 810247419084c82d 
 * abababab, 46c9e2ad5b69bffd 
 * abcabcab, 848d93ed4ee299c4 
 * fifififi, bf47e730aa1f332d
 * aaaabbbb, c622054d9e6f17b4 
 * gggggggg, 9c72446df124ddf2 
 * --------------------
 * oneway Ticket 
 * cccccccc, bee397acc400449e 
 * dddddddd, ef800207a3648c7c 
 * cdcdcdcd, 3d403bec6086580c 
 * ccccdddd, 4faddd8d57ccac53 
 * cccdddcc, eff3fde7bd339c10
 * ccddccdd, 237c9f7bc06d1ad8 
 * ddddcccc, 6a77292a77c512b0 
 * dcdcdcdc,20ff5ee5ed9f9bcf 
 * -------------------- 
 * Prepaid Card 
 * NMNMNMNM, ce8088fa59498d1a
 * MMMMMMMM, f05376020754754c 
 * NNNNNNNN, 46374f63a5d96dc0 
 * SASASASA, ea6844318aa97fcf 
 * GBGBGBGB, 936b4877b5d2ca10 
 * FGFGFGFG, bdf027e7da8daf15
 * ERERERER, 812aec7102107761 
 * EEEERRRR, ce779ddf85d9dabc
 */
