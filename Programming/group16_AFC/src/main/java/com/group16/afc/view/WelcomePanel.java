package com.group16.afc.view;

import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.group16.afc.manager.AFCManagerCardIn;
import com.group16.afc.manager.AFCManagerCardOut;
import com.group16.afc.manager.AFCManagerTicket24hIn;
import com.group16.afc.manager.AFCManagerTicket24hOut;
import com.group16.afc.manager.AFCManagerTicketOneWayIn;
import com.group16.afc.manager.AFCManagerTicketOneWayOut;
import com.group16.afc.manager.AFCState;
import com.group16.afc.model.Direction;

/**
 * 
 * @author nguyen huu thang
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.view
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description panel input ticket bar code
 */
public class WelcomePanel extends BasePanel {
	private static final int ROW_1 = 150;
	private static final int ROW_2 = 250;
	private static final int ROW_3 = 400;
	private static final int COL_1 = 50;
	private static final int COL_2 = 300;

	private JLabel lbTitle;
	private JLabel lbText;
	private JTextField tfTicketId;
	private JButton nextButton;

	private boolean isFindTicket;

	@Override
	public void initComponents() {
		setLayout(null);
		lbTitle = setTitle("Welcome");
		lbText = setTextLabel(COL_1, ROW_1, 300, 80, "Recognize your ticket ID");
		tfTicketId = setJTextField(COL_1, ROW_2, 500, 100, "");
		nextButton = setJButton(COL_1, ROW_3, "Next");
		isFindTicket = false;
	}

	@Override
	public void registerListener() {
		// TODO Auto-generated method stub
		nextButton.addMouseListener(mouseAdapter);
	}

	@Override
	public void addComponents() {
		// TODO Auto-generated method stub
		add(lbTitle);
		add(lbText);
		add(tfTicketId);
		add(nextButton);
	}

	@Override
	protected void clickedManipulation(MouseEvent e) {
		isFindTicket = false;
		super.clickedManipulation(e);
		String barCode = tfTicketId.getText();
		if (barCode == "" || barCode == null) return;

		if (barCode.length() < 8 || barCode.length() > 8) {
			myContainer.showInvalid(false);
			return;
		}
		if (barCode.length() == 8) {
			char[] charBarcode = barCode.toCharArray();
			int countLowerCase = 0;
			for (Character c : charBarcode) {

				if (Character.isLetter(c) && Character.isLowerCase(c)) countLowerCase += 1;
			}
			if (countLowerCase != 0 && countLowerCase != 8) {
				System.out.println(countLowerCase);
				myContainer.showInvalid(false);
				return;
			} else if (countLowerCase == 0) {
				if (!isFindTicket) validCard(barCode);
			} else {
				if (!isFindTicket) validTicketOW(barCode);
				if (!isFindTicket) validTicketTF(barCode);
			}
			if (!isFindTicket) myContainer.showInvalid(true);
		}

	}

	private void validCard(String barCode) {
		if (AFCGUI.getInstance().afc.getDirection() == Direction.IN) {
			// new controller
			AFCManagerCardIn afcManager = new AFCManagerCardIn();
			// controller set afc
			afcManager.setAfc(AFCGUI.getInstance().afc);
			// controller set barcode
			afcManager.setBarcode(barCode);
			// controller trans barcode to code
			if (afcManager.validateTicket()) {

				// controller get infor of ticket
				if (afcManager.getInforTicket()) {
					// mark the trans is done
					isFindTicket = true;
					// check ticket is availble
					afcManager.isAvailableTicket();
					if (afcManager.getState() == AFCState.WRONG_DIRECTION_NOT_IN
							|| afcManager.getState() == AFCState.WRONG_DIRECTION_NOT_OUT) {
						myContainer.showWrongDirection(afcManager.getState());
						return;
					}
					myContainer.showCard(afcManager.getState(), afcManager.prepaidCard);
					afcManager.updateInfoTicket();
					afcManager.controlGate();

				}

			}

		}

		if (AFCGUI.getInstance().afc.getDirection() == Direction.OUT) {
			// new controller
			AFCManagerCardOut afcManager = new AFCManagerCardOut();
			// controller set afc
			afcManager.setAfc(AFCGUI.getInstance().afc);
			// controller set barcode
			afcManager.setBarcode(barCode);
			// controller trans barcode to code
			if (afcManager.validateTicket()) {

				// controller get infor of ticket
				if (afcManager.getInforTicket()) {
					// mark the trans is done
					isFindTicket = true;
					// check ticket is availble
					afcManager.isAvailableTicket();
					if (afcManager.getState() == AFCState.WRONG_DIRECTION_NOT_IN
							|| afcManager.getState() == AFCState.WRONG_DIRECTION_NOT_OUT) {
						myContainer.showWrongDirection(afcManager.getState());
						return;
					}

					myContainer.showCard(afcManager.getState(), afcManager.prepaidCard);
					afcManager.updateInfoTicket();
					afcManager.controlGate();

				}

			}

		}
	}

	private void validTicketTF(String barCode) {
		if (AFCGUI.getInstance().afc.getDirection() == Direction.IN) {
			// new controller
			AFCManagerTicket24hIn afcManager = new AFCManagerTicket24hIn();
			// controller set afc
			afcManager.setAfc(AFCGUI.getInstance().afc);
			// controller set barcode
			afcManager.setBarcode(barCode);
			// controller trans barcode to code
			if (afcManager.validateTicket()) {

				// controller get infor of ticket
				if (afcManager.getInforTicket()) {
					// mark the trans is done
					isFindTicket = true;
					// check ticket is availble
					afcManager.isAvailableTicket();

					if (afcManager.getState() == AFCState.WRONG_DIRECTION_NOT_IN
							|| afcManager.getState() == AFCState.WRONG_DIRECTION_NOT_OUT) {
						myContainer.showWrongDirection(afcManager.getState());
						return;
					}
					myContainer.showOneDay(afcManager.getState(), afcManager.ticket24h);
					afcManager.updateInfoTicket();
					afcManager.controlGate();
				}

			}

		}

		if (AFCGUI.getInstance().afc.getDirection() == Direction.OUT) {
			// new controller
			AFCManagerTicket24hOut afcManager = new AFCManagerTicket24hOut();
			// controller set afc
			afcManager.setAfc(AFCGUI.getInstance().afc);
			// controller set barcode
			afcManager.setBarcode(barCode);
			// controller trans barcode to code
			if (afcManager.validateTicket()) {

				// controller get infor of ticket
				if (afcManager.getInforTicket()) {
					// mark the trans is done
					isFindTicket = true;
					// check ticket is availble
					afcManager.isAvailableTicket();

					if (afcManager.getState() == AFCState.WRONG_DIRECTION_NOT_IN
							|| afcManager.getState() == AFCState.WRONG_DIRECTION_NOT_OUT) {
						myContainer.showWrongDirection(afcManager.getState());
						return;
					}
					myContainer.showOneDay(afcManager.getState(), afcManager.ticket24h);
					afcManager.updateInfoTicket();
					afcManager.controlGate();
				}

			}

		}
	}

	private void validTicketOW(String barCode) {
		if (AFCGUI.getInstance().afc.getDirection() == Direction.IN) {
			// new controller
			AFCManagerTicketOneWayIn afcManager = new AFCManagerTicketOneWayIn();
			// controller set afc
			afcManager.setAfc(AFCGUI.getInstance().afc);
			// controller set barcode
			afcManager.setBarcode(barCode);
			// controller trans barcode to code
			if (afcManager.validateTicket()) {

				// controller get infor of ticket
				if (afcManager.getInforTicket()) {
					// mark the trans is done
					isFindTicket = true;
					// check ticket is availble
					afcManager.isAvailableTicket();
					if (afcManager.getState() == AFCState.WRONG_DIRECTION_NOT_IN
							|| afcManager.getState() == AFCState.WRONG_DIRECTION_NOT_OUT) {
						myContainer.showWrongDirection(afcManager.getState());
						return;
					}
					myContainer.showOneWay(afcManager.getState(), afcManager.ticketOW);
					afcManager.updateInfoTicket();
					afcManager.controlGate();

					return;
				}

			}

		}

		if (AFCGUI.getInstance().afc.getDirection() == Direction.OUT) {
			AFCManagerTicketOneWayOut afcManager = new AFCManagerTicketOneWayOut();
			// controller set afc
			afcManager.setAfc(AFCGUI.getInstance().afc);
			// controller set barcode
			afcManager.setBarcode(barCode);
			// controller trans barcode to code
			if (afcManager.validateTicket()) {

				// controller get infor of ticket
				if (afcManager.getInforTicket()) {
					// mark the trans is done
					isFindTicket = true;
					// check ticket is availble
					afcManager.isAvailableTicket();

					if (afcManager.getState() == AFCState.WRONG_DIRECTION_NOT_IN
							|| afcManager.getState() == AFCState.WRONG_DIRECTION_NOT_OUT) {
						myContainer.showWrongDirection(afcManager.getState());
						return;
					}
					myContainer.showOneWay(afcManager.getState(), afcManager.ticketOW);
					afcManager.updateInfoTicket();
					afcManager.controlGate();

					return;
				}

			}

		}

		return;
	}
}
