package com.group16.afc.view;

/**
 * 
 * @author nguyen huu thang
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.view
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description define default for a panel
 */
public interface Setup {
	void initComponents();

	void registerListener();

	void addComponents();
}
