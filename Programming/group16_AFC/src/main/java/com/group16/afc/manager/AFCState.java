package com.group16.afc.manager;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.manager
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description
 * list AFC state
 * <p>
 * the information is sent for view
 */
public enum AFCState {
	NEW, BARCODE_INVALID, BARCODE_VALID, GET_TICKET_CODE, GET_INFO_SUCCESS, GET_INFO_FAIL, AVAILBLE_TICKET,
	UNAVAILBLE_TICKET, // unkown error make ticket unavailble
	UPDATE_SUCCESS, UPDATE_FAIL, GATE_OPEN, GATE_CLOSE, DONE, NOT_ENOUGH_BASE_BALANCE, // when you check in by PC
	NOT_ENOUGH_BALANCE, // when you check out by PC, OW
	WRONG_DIRECTION_NOT_IN, // when your ticket is only availbe for Check-out
	WRONG_DIRECTION_NOT_OUT, // when your ticket is only availbe for check-in
	EXPRIED_TICKET, // when your ticket (TF,OW,PC) is out of use
	EXPRIED_TF_BUT_NOT_UPDATE, WRONG_STATION, // when your OW is out of inStation and outStation

	NULL
}
