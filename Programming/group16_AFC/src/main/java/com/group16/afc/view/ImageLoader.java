package com.group16.afc.view;

import java.awt.Image;
import javax.swing.ImageIcon;

/**
 * 
 * @author nguyen huu thang
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.view
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description Load image
 */
public class ImageLoader {
	public static final Image BAR = getImage("button.png");

	public static Image getImage(String name) {
		String path = ImageLoader.class.getResource("/res/drawable/" + name).getPath();
		return new ImageIcon(path).getImage();
	}
}
