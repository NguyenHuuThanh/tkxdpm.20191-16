package com.group16.afc.model;

import java.time.LocalDateTime;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.model
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description store data of TF
 */
public class Ticket24h extends Ticket {
	/**
	 * start Time
	 */
	protected LocalDateTime startTime;
	/**
	 * expried Time
	 */
	protected LocalDateTime expTime;
	/**
	 * status
	 */
	private Ticket24hStatus status;

	public Ticket24h() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getExpTime() {
		return expTime;
	}

	public void setExpTime(LocalDateTime expTime) {
		this.expTime = expTime;
	}

	public Ticket24hStatus getStatus() {
		return status;
	}

	public void setStatus(Ticket24hStatus status) {
		this.status = status;
	}

	public void setNewTravelInpoint(Station startStation) {
		Travel newTravel = new Travel();
		newTravel.inStation = startStation;
		this.newestTravel = newTravel;
	}

	public void setNewestTravelOutPoint(Station stopStation) {
		this.newestTravel.setOutStation(stopStation);
	}

	@Override
	public String toString() {
		return "Ticket24h [startTime=" + startTime + ", expTime=" + expTime + ", newestTravels=" + newestTravel
				+ ", status=" + status + ", id=" + id + ", timeBuy=" + timeBuy + ", code=" + code + "]";
	}

	@Override
	public boolean isOneDay() {
		// TODO Auto-generated method stub
		return true;
	}
}
