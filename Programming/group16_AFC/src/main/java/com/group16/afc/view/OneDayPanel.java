package com.group16.afc.view;

import java.awt.event.MouseEvent;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.swing.JLabel;

import com.group16.afc.manager.AFCState;
import com.group16.afc.model.Ticket24h;

/**
 * 
 * @author nguyen huu thang
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.view
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description detail creen for 24h ticket
 */
public class OneDayPanel extends BasePanel {
	private static final int ROW_1 = 100;
	private static final int ROW_2 = 200;
	private static final int ROW_3 = 300;
	private static final int ROW_4 = 400;
	private static final int COL_1 = 50;
	private static final int COL_2 = 300;
	private static final int WDITH = 600;

	private JLabel lbID, lbValidUntil, lbEnterTime, lbStation;
	private JLabel title;
	private JLabel lbIDValue, lbValidValue, lbEnterTimeValue, lbStationValue;

	@Override
	public void initComponents() {
		setLayout(null);
		title = setTitle("");
		lbID = setTextLabel(COL_1, ROW_1, WDITH, 80, "ID");
		lbValidUntil = setTextLabel(COL_1, ROW_2, WDITH, 80, "Valid Until");
		lbEnterTime = setTextLabel(COL_1, ROW_3, WDITH, 80, "Enter Time");
		lbStation = setTextLabel(COL_1, ROW_4, WDITH, 80, "Station");
		lbIDValue = setTextLabel(COL_2, ROW_1, WDITH, 80, "1");
		lbValidValue = setTextLabel(COL_2, ROW_2, WDITH, 80, "1");
		lbEnterTimeValue = setTextLabel(COL_2, ROW_3, WDITH, 80, "1");
		lbStationValue = setTextLabel(COL_2, ROW_4, WDITH, 80, "1");
	}

	/**
	 * display 24h ticket detail
	 * 
	 * @param afcstate afc machine state
	 * @param ticket   24h ticket is used
	 */
	public void setOneDayInfo(AFCState afcstate, Ticket24h ticket) {
		if (afcstate == AFCState.EXPRIED_TICKET || afcstate == AFCState.EXPRIED_TF_BUT_NOT_UPDATE) {
			title.setText("Expried Ticket");
		} else if (afcstate == AFCState.AVAILBLE_TICKET) {
			title.setText("Opening Gate With One-Day Ticket");
		}
		lbIDValue.setText(ticket.getId());
		if (ticket.getExpTime() != null)
			lbValidValue.setText(ticket.getExpTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		else
			lbValidValue.setText(
					ticket.getStartTime().plusDays(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		lbEnterTimeValue.setText(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		// TODO set current station
		lbStationValue.setText(AFCGUI.getInstance().afc.getStation().getName());
	}

	@Override
	public void registerListener() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void clickedManipulation(MouseEvent e) {
		// TODO Auto-generated method stub
		super.clickedManipulation(e);

	}

	@Override
	public void addComponents() {
		// TODO Auto-generated method stub
		add(title);
		add(lbID);
		add(lbValidUntil);
		add(lbEnterTime);
		add(lbStation);
		add(lbIDValue);
		add(lbValidValue);
		add(lbEnterTimeValue);
		add(lbStationValue);
		addBackButton();
	}

}
