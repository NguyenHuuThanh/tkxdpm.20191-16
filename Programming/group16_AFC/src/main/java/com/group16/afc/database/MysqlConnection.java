package com.group16.afc.database;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.cj.jdbc.ConnectionImpl;
import com.sun.jdi.connect.spi.Connection;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.database
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description Create Connection as only 1 instance using Singleton Pattern
 */
public class MysqlConnection extends Connection {

	private String url;

	private String user;

	private String password;

	private ConnectionImpl connection;

	private static MysqlConnection instance = null;

	private MysqlConnection() throws SQLException {

		url = "jdbc:mysql://localhost:3306/mydb";

		user = "root";

		password = "";

		connection = (ConnectionImpl) DriverManager.getConnection(url, user, password);
	}

	/**
	 * get Instance of Connection
	 * 
	 * @return {@link MysqlConnection}
	 * @throws SQLException when SQL is not ready
	 */
	public static MysqlConnection getInstance() throws SQLException {
		if (instance == null) {
			instance = new MysqlConnection();
		}
		return instance;
	}

	@Override
	public byte[] readPacket() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void writePacket(byte[] pkt) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void close() throws IOException {
		instance.close();
	}

	@Override
	public boolean isOpen() {
		// TODO Auto-generated method stub
		return instance.isOpen();
	}

	public ConnectionImpl getConnection() {
		return connection;
	}

}
