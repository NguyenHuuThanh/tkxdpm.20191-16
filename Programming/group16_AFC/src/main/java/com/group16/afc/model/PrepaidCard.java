package com.group16.afc.model;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.model
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description store data of PC
 */
public class PrepaidCard extends Ticket {
	/**
	 * base balance of PC
	 */
	public static final double baseBalance = 10000;
	/**
	 * remaining cash of PC
	 */
	protected double cash;
	/**
	 * {@link PrepaidCardStatus} of PC
	 */
	private PrepaidCardStatus status;

	public PrepaidCard() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PrepaidCard(double cash) {
		super();
		this.cash = cash;
	}

	public PrepaidCard(String id, double cash) {
		super(id);
		this.cash = cash;
	}

	public double getCash() {
		return cash;
	}

	public void setCash(double cash) {
		this.cash = cash;
	}

	public PrepaidCardStatus getStatus() {
		return status;
	}

	public void setStatus(PrepaidCardStatus status) {
		this.status = status;
	}

	public void setNewTravelInpoint(Station startStation) {
		Travel newTravel = new Travel();
		newTravel.inStation = startStation;
		this.newestTravel = newTravel;
	}

	public void setNewestTravelOutPoint(Station stopStation) {
		this.newestTravel.setOutStation(stopStation);
	}

	@Override
	public String toString() {
		return "PrepaidCard [cash=" + cash + ", status=" + status + ", id=" + id + ", timeBuy=" + timeBuy + ", code="
				+ code + "]";
	}

	@Override
	public boolean isCard() {
		// TODO Auto-generated method stub
		return true;
	}

}
