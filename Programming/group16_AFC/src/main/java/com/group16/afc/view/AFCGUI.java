package com.group16.afc.view;

import java.awt.CardLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import com.group16.afc.manager.AFCManager;
import com.group16.afc.model.AFC;

/**
 * 
 * @author nguyen huu thang
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.view
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description AFCGUI is singleton, the Jframe when program start
 */
public class AFCGUI extends JFrame implements Setup {
	public static final int WIDTH_FRAME = 805;
	public static final int HEIGHT_FRAME = 700;

	private MyContainer myContainer;
	public AFCManager afcManager;
	public AFC afc;

	private static AFCGUI sInstance = null;

	public static AFCGUI getInstance() {
		if (sInstance == null) {
			sInstance = new AFCGUI();
		}
		return sInstance;
	}

	private AFCGUI() {
		initComponents();
		registerListener();
		addComponents();
	}

	@Override
	public void initComponents() {
		setLayout(new CardLayout());
		setSize(WIDTH_FRAME, HEIGHT_FRAME);
		setResizable(false);
		setLocationRelativeTo(null);
		setTitle("AFC Client");
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		myContainer = new MyContainer();
		myContainer.setGui(this);
	}

	@Override
	public void registerListener() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				e.getWindow().dispose();
			}
		});
	}

	@Override
	public void addComponents() {
		add(myContainer);
	}

}
