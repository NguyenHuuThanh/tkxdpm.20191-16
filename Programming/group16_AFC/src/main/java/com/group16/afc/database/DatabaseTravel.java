package com.group16.afc.database;

import java.time.LocalDateTime;

import com.group16.afc.model.Travel;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.database
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description Travel DAO
 */
public interface DatabaseTravel {

	/**
	 * get {@link Travel} by id
	 * 
	 * @param _ticketID {@link String}
	 * @return {@link Travel}
	 */
	public Travel getTravel(String _ticketID);

	/**
	 * update out Point of {@link Travel}
	 * 
	 * @param outpontId int
	 * @param travelId int
	 * @return <code>true</code> if complete
	 */
	public boolean updateTravelOutPoint(int outpontId, int travelId);

	/**
	 * update end time of {@link Travel}
	 * 
	 * @param endTime {@link LocalDateTime}
	 * @param travelId int
	 * @return <code>true</code> if complete
	 */
	public boolean updateTravelEndTime(LocalDateTime endTime, int travelId);

	/**
	 * insert new {@link Travel} to database
	 * 
	 * @param ticketId {@link String}
	 * @param stationId int 
	 * @param startTime {@link LocalDateTime}
	 * @return <code>true</code> if complete
	 */
	public boolean insertNewTravel(String ticketId, int stationId, LocalDateTime startTime);
}
