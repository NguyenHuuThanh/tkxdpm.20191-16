package com.group16.afc.database;

import com.group16.afc.model.PrepaidCard;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.database
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description TicketPC DAO
 */
public interface DatabasePC {
	/**
	 * 
	 * @param _code {@link String} is matched with barcode
	 * @return {@link PrepaidCard}
	 */
	public PrepaidCard getPC(String _code);

	/**
	 * update Status of PC
	 * 
	 * @param cardId {@link String}
	 * @param status {@link String}
	 * @return <code>true</code> if complete
	 */
	public boolean updatePCStatus(String cardId, String status);

	/**
	 * update remaining Cash of PC
	 * 
	 * @param cardId {@link String}
	 * @param cash {@link String}
	 * @return <code>true</code> if complete
	 */
	public boolean updatePCCash(String cardId, double cash);
}
