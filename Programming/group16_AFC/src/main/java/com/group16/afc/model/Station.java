package com.group16.afc.model;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.model
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description store data of {@link Station}
 */
public class Station {
	/**
	 * id of Station
	 */
	private int id;
	/**
	 * name of Station
	 */
	private String name;

	/**
	 * distance form this Station to Station id 0
	 */
	private double distance;

	public Station() {
		super();
	}

	public Station(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	/**
	 * @param id       int
	 * @param name     {@link String}
	 * @param distance double
	 */
	public Station(int id, String name, double distance) {
		super();
		this.id = id;
		this.name = name;
		this.distance = distance;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	@Override
	public String toString() {
		return "Station [id=" + id + ", name=" + name + ", distance=" + distance + "]";
	}

}
