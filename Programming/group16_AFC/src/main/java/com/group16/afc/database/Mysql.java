package com.group16.afc.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import com.group16.afc.model.PrepaidCard;
import com.group16.afc.model.PrepaidCardStatus;
import com.group16.afc.model.Station;
import com.group16.afc.model.Ticket24h;
import com.group16.afc.model.Ticket24hStatus;
import com.group16.afc.model.TicketOneWay;
import com.group16.afc.model.TicketOneWayStatus;
import com.group16.afc.model.Travel;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.database
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description
 * implement of {@link Database}.
 * <p>
 * use Singleton Pattern
 */
public class Mysql implements Database {

	/**
	 * store connection
	 */
	private Connection con;

	private static Mysql instance = null;

	/**
	 * @throws SQLException Constructor
	 */
	private Mysql() throws SQLException {
		super();
		con = (Connection) MysqlConnection.getInstance().getConnection();
	}

	/**
	 * get instance of {@link Mysql}
	 * 
	 * @return instance {@link Mysql}
	 * @throws SQLException when SQL is not ready
	 */
	public static Mysql getInstance() throws SQLException {
		if (instance == null) {
			instance = new Mysql();
		}
		return instance;
	}

	public Travel getTravel(String _ticketID) {
		Travel s;
		try {
//			Connection con = DriverManager.getConnection(url, user, password);
			String strx;
			if (_ticketID.contains("TF"))
				strx = "ticket24h_id";
			else if (_ticketID.contains("OW"))
				strx = "ticketOneWay_id";
			else if (_ticketID.contains("PC"))
				strx = "Card_id";
			else
				return null;
			PreparedStatement stmt = con.prepareStatement(
					"select * from travel " + " where " + strx + "  = ? " + " order by startTime DESC limit 1");
			stmt.setString(1, _ticketID);

			ResultSet rs = stmt.executeQuery();

			// lay ket qua tim duoc dau tien
			if (rs.next()) {

				s = new Travel();

				Integer id = rs.getInt(1);
				s.setId(id);

				int inPointID = rs.getInt("inPoint_id");
				Station inStation = Mysql.getInstance().getStation(inPointID);
				s.setInStation(inStation);

				int outPointID = rs.getInt("outPoint_id");
				Station outStation = Mysql.getInstance().getStation(outPointID);
				s.setOutStation(outStation);

				Timestamp time1 = rs.getTimestamp("startTime");
				if (time1 != null) {
					LocalDateTime startTime = time1.toLocalDateTime();
					s.setStartTime(startTime);
				}

				Timestamp time2 = rs.getTimestamp("endTime");
				if (time2 != null) {
					LocalDateTime expTime = time2.toLocalDateTime();
					s.setExpTime(expTime);
				}

			} else {
				s = null;
			}

//			con.close();
			return s;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Station getStation(String name) {
		Station s;
		try {
//			Connection con = DriverManager.getConnection(url, user, password);

			PreparedStatement stmt = con.prepareStatement("select * from point " + " where name = ? ");

			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();

			/*
			 * or you can write Statement stmt = con.createStatement(); ResultSet rs =
			 * stmt.executeQuery("select * from point " + " where name = '" + name + "' ");
			 */

			// lay ket qua tim duoc dau tien
			if (rs.next()) {

				s = new Station();
				Integer id = rs.getInt(1);
				s.setId(id);

				String names = rs.getString(2);
				s.setName(names);

				Double distance = rs.getDouble(3);
				s.setDistance(distance);
			} else {
				s = null;
			}

//			con.close();
			return s;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Station getStation(int _id) {
		Station s;
		try {
//            Connection con = DriverManager.getConnection(url, user, password);

			PreparedStatement stmt = con.prepareStatement("select * from point " + " where id = ? ");
			stmt.setInt(1, _id);
			ResultSet rs = stmt.executeQuery();

			// lay ket qua tim duoc dau tien
			if (rs.next()) {

				s = new Station();
				Integer id = rs.getInt(1);
				s.setId(id);

				String names = rs.getString(2);
				s.setName(names);

				Double distance = rs.getDouble(3);
				s.setDistance(distance);
			} else {
				s = null;
			}

//            con.close();
			return s;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Ticket24h getTF(String _code) {
		Ticket24h s;
		try {
//			Connection con = DriverManager.getConnection(url, user, password);

			PreparedStatement stmt = con.prepareStatement("select * from ticket24h " + " where code = ? ");

			stmt.setString(1, _code);
			ResultSet rs = stmt.executeQuery();

			/*
			 * or you can write Statement stmt = con.createStatement(); ResultSet rs =
			 * stmt.executeQuery("select * from point " + " where name = '" + name + "' ");
			 */

			// lay ket qua tim duoc dau tien
			if (rs.next()) {

				s = new Ticket24h();
				String id = rs.getString(1);
				s.setId(id);

				String code = rs.getString(2);
				s.setCode(code);

				Timestamp time1 = rs.getTimestamp(3);
				if (time1 != null) {
					LocalDateTime timeBuy = time1.toLocalDateTime();
					s.setTimeBuy(timeBuy);
				}

				Timestamp time2 = rs.getTimestamp(4);
				if (time2 != null) {
					LocalDateTime startTime = time2.toLocalDateTime();
					s.setStartTime(startTime);
				}

				Timestamp time3 = rs.getTimestamp(5);
				if (time3 != null) {
					LocalDateTime expTime = time3.toLocalDateTime();
					s.setExpTime(expTime);
				}

				String status = rs.getString(6);
				if (status != null) for (Ticket24hStatus sta : Ticket24hStatus.values()) {
					if (status.compareTo(sta.toString()) == 0) {
						s.setStatus(sta);
					}
				}

			} else {
				s = null;
			}

//			con.close();
			return s;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public TicketOneWay getOW(String _code) {
		TicketOneWay s;
		try {
//			Connection con = DriverManager.getConnection(url, user, password);

			PreparedStatement stmt = con.prepareStatement("select * from ticketoneway " + " where code = ? ");

			stmt.setString(1, _code);
			ResultSet rs = stmt.executeQuery();

			/*
			 * or you can write Statement stmt = con.createStatement(); ResultSet rs =
			 * stmt.executeQuery("select * from point " + " where name = '" + name + "' ");
			 */

			// lay ket qua tim duoc dau tien
			if (rs.next()) {

				s = new TicketOneWay();
				String id = rs.getString(1);
				s.setId(id);

				String code = rs.getString(2);
				s.setCode(code);
				Timestamp time1 = rs.getTimestamp(3);
				if (time1 != null) {
					LocalDateTime timeBuy = time1.toLocalDateTime();
					s.setTimeBuy(timeBuy);
				}

				String status = rs.getString(4);
				for (TicketOneWayStatus sta : TicketOneWayStatus.values()) {
					if (status.compareTo(sta.toString()) == 0) {
						s.setStatus(sta);
					}
				}

				s.setInStation(getStation(rs.getInt(5)));
				s.setOutStation(getStation(rs.getInt(6)));
			} else {
				s = null;
			}

//			con.close();
			return s;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public PrepaidCard getPC(String _code) {
		PrepaidCard s;
		try {
//			Connection con = DriverManager.getConnection(url, user, password);

			PreparedStatement stmt = con.prepareStatement("select * from card " + " where code = ? ");

			stmt.setString(1, _code);
			ResultSet rs = stmt.executeQuery();

			/*
			 * or you can write Statement stmt = con.createStatement(); ResultSet rs =
			 * stmt.executeQuery("select * from point " + " where name = '" + name + "' ");
			 */

			// lay ket qua tim duoc dau tien
			if (rs.next()) {

				s = new PrepaidCard();
				String id = rs.getString(1);
				s.setId(id);

				String code = rs.getString(2);
				s.setCode(code);

				Timestamp time1 = rs.getTimestamp(3);
				if (time1 != null) {
					LocalDateTime timeBuy = time1.toLocalDateTime();
					s.setTimeBuy(timeBuy);
				}

				double cash = rs.getDouble(4);
				s.setCash(cash);

				String status = rs.getString(5);
				if (status != null) for (PrepaidCardStatus sta : PrepaidCardStatus.values()) {
					if (status.compareTo(sta.toString()) == 0) {
						s.setStatus(sta);
					}
				}

			} else {
				s = null;
			}

//			con.close();
			return s;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void main(String[] args) throws SQLException {

	}

	public boolean updateTravelOutPoint(int outpontId, int travelId) {
		try {
//			Connection con = DriverManager.getConnection(url, user, password);

			PreparedStatement stmt = con.prepareStatement("update travel set outPoint_id = ? where id = ?");

			stmt.setString(1, outpontId + "");
			stmt.setString(2, travelId + "");
			int a = stmt.executeUpdate();
			stmt.close();

//			con.close();
			return a != 0;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean updateTravelEndTime(LocalDateTime endTime, int travelId) {
		try {
//			Connection con = DriverManager.getConnection(url, user, password);

			PreparedStatement stmt = con.prepareStatement("update travel set endTime = ? where id = ?");

			stmt.setString(1, endTime.toLocalDate() + " " + endTime.toLocalTime());
			stmt.setString(2, travelId + "");
			int a = stmt.executeUpdate();
			stmt.close();

//			con.close();
			return a != 0;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean insertNewTravel(String ticketId, int stationId, LocalDateTime startTime) {
		try {
//			Connection con = DriverManager.getConnection(url, user, password);
			String idType = "";
			if (ticketId.contains("TF"))
				idType = "ticket24h_id";
			else if (ticketId.contains("OW"))
				idType = "ticketOneWay_id";
			else
				idType = "Card_id";
			PreparedStatement stmt = con.prepareStatement(
					"insert into travel (" + idType + ", typeTicket, inPoint_id, startTime) " + " VALUES (?,?,?,?)");
			stmt.setString(1, ticketId);
			stmt.setString(2, "2");
			stmt.setInt(3, stationId);

			stmt.setString(4, startTime.toLocalDate() + " " + startTime.toLocalTime());

			int a = stmt.executeUpdate();
			stmt.close();
//			con.close();
			return a != 0;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean updatePCStatus(String cardId, String status) {
		try {
//			Connection con = DriverManager.getConnection(url, user, password);

			PreparedStatement stmt = con.prepareStatement("update card set status = ? where id = ?");

			stmt.setString(1, status + "");
			stmt.setString(2, cardId + "");
			int a = stmt.executeUpdate();
			stmt.close();

//			con.close();
			return a != 0;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean updatePCCash(String cardId, double cash) {
		try {
//			Connection con = DriverManager.getConnection(url, user, password);

			PreparedStatement stmt = con.prepareStatement("update card set cash = ? where id = ?");

			stmt.setDouble(1, cash);
			stmt.setString(2, cardId);
			int a = stmt.executeUpdate();
			stmt.close();

//			con.close();
			return a != 0;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean updateTFTime(String ticketId, LocalDateTime startTime) {
		try {
//			Connection con = DriverManager.getConnection(url, user, password);

			PreparedStatement stmt = con
					.prepareStatement("update ticket24h set startTime = ?, expTime = ? where id = ?");

			stmt.setString(1, startTime.toLocalDate() + " " + startTime.toLocalTime());
			stmt.setString(2, startTime.plusDays(1).toLocalDate() + " " + startTime.plusDays(1).toLocalTime());
			stmt.setString(3, ticketId);
			int a = stmt.executeUpdate();
			stmt.close();

//			con.close();
			return a != 0;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean updateTFStatus(String ticketId, String status) {
		try {
//			Connection con = DriverManager.getConnection(url, user, password);

			PreparedStatement stmt = con.prepareStatement("update ticket24h set status = ? where id = ?");

			stmt.setString(1, status + "");
			stmt.setString(2, ticketId + "");
			int a = stmt.executeUpdate();
			stmt.close();

//			con.close();
			return a != 0;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean updateOWStatus(String ticketId, String status) {
		try {
//			Connection con = DriverManager.getConnection(url, user, password);

			PreparedStatement stmt = con.prepareStatement("update ticketoneway set status = ? where id = ?");

			stmt.setString(1, status + "");
			stmt.setString(2, ticketId + "");
			int a = stmt.executeUpdate();
			stmt.close();

//			con.close();
			return a != 0;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
}
