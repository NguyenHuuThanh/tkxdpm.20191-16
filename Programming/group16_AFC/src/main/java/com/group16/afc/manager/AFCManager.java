package com.group16.afc.manager;

import com.group16.afc.database.Database;
import com.group16.afc.model.AFC;
import com.group16.afc.model.Ticket;
import config.Configuration;
import hust.soict.se.gate.Gate;
import utils.CalculatingBalanceInterface;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package com.group16.afc.manager
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description
 * AFC Manager Standard
 * <p>
 * main controller for each ticket
 */
public abstract class AFCManager {

	protected AFC afc;

	protected Gate gate;

	protected Ticket ticket;

	protected String code;

	protected String barcode;

	protected AFCState state;

	protected Database database;

	protected CalculatingBalanceInterface caculateBalance;

	/**
	 * this method to tranform barcode to ticket code
	 * 
	 * @return {@link Boolean} 
	 * <code>true</code> if barcode is matched with code in database
	 * <code>false</code> if not
	 */
	public abstract boolean validateTicket();

	/**
	 * this method to get ticket info from DB
	 * 
	 * @return <code>true</code> if complete
	 */
	public abstract boolean getInforTicket();

	/**
	 * this method is check ticket is availble to use
	 * 
	 * @return <code>true</code> if complete
	 */
	public abstract boolean isAvailableTicket();

	/**
	 * this method to update change to DB
	 * 
	 * @return <code>true</code> if complete
	 */
	public abstract boolean updateInfoTicket();

	/**
	 * this method to control open and close Gate
	 * 
	 * @return <code>true</code> if complete
	 */
	public abstract boolean controlGate();

	/**
	 * this method is process new ticket
	 */
	public abstract void processTicket();

	/**
	 * Constructor
	 * <p>
	 * for set Calculating Balance Method
	 * <p>
	 * for set type of DAO is use
	 */
	public AFCManager() {
		super();
		caculateBalance = Configuration.getInstance().getCalBalance();

		setDatabase(Configuration.getInstance().getDb());

	}

	public AFC getAfc() {
		return afc;
	}

	public void setAfc(AFC afc) {
		this.afc = afc;
	}

	public Gate getGate() {
		return gate;
	}

	public void setGate(Gate gate) {
		this.gate = gate;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public AFCState getState() {
		return state;
	}

	public void setState(AFCState state) {
		this.state = state;
	}

	public Database getDatabase() {
		return database;
	}

	public void setDatabase(Database database) {
		this.database = database;
	}

	public CalculatingBalanceInterface getCaculateBalance() {
		return caculateBalance;
	}

	public void setCaculateBalance(CalculatingBalanceInterface caculateBalance) {
		this.caculateBalance = caculateBalance;
	}

}
