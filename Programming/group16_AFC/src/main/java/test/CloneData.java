package test;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;
import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.recognizer.TicketRecognizer;
import hust.soict.se.scanner.CardScanner;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package test
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description clone fake barcode and code compatible
 */
public class CloneData {

	public static String padINT(Integer num) {
		String str = new String();
		if (num >= 0 && num <= 10000) {
			if (num < 10)
				str = "000" + num;

			else if (num < 100)
				str = "00" + num;
			else if (num < 1000)
				str = "0" + num;
			else
				str = "" + num;
		}

		return str;
	};

	public static void cloneTicketID(LocalDate startDay, LocalDate endDay, String typeTicket, int numberOfRecord,
			int startID) {
		Random random = new Random();
		int minDay = (int) startDay.toEpochDay();
		int maxDay = (int) endDay.toEpochDay();
		for (int i = startID; i < startID + numberOfRecord; ++i) {
			long randomDay = minDay + random.nextInt(maxDay - minDay);

			LocalDate randomBirthDate = LocalDate.ofEpochDay(randomDay);
			String date = randomBirthDate.toString().replace("-", "");
			System.out.println(typeTicket + date + padINT(i));

		}
	}

	public static void cloneTicketCodeByBarCode(LinkedList<String> barcodes) {
		Iterator<String> it = barcodes.iterator();
		TicketRecognizer t = TicketRecognizer.getInstance();
		while (it.hasNext()) {
			try {
				String barcode = it.next();
				String ticketcode = t.process(barcode);
				System.out.println(barcode + ", " + ticketcode);
			} catch (InvalidIDException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public static void cloneCardCodeByBarCode(LinkedList<String> barcodes) {
		Iterator<String> it = barcodes.iterator();
		CardScanner t = CardScanner.getInstance();
		while (it.hasNext()) {
			try {
				String barcode = it.next();
				String cardcode = t.process(barcode);
				System.out.println(barcode + ", " + cardcode);
			} catch (InvalidIDException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public static void main(String[] args) {

		// test clone ticket id
		CloneData.cloneTicketID(LocalDate.of(2019, 10, 10), LocalDate.of(2019, 10, 20), "OW", 8, 20);
		System.out.println("--------------------");
		CloneData.cloneTicketID(LocalDate.of(2019, 10, 10), LocalDate.of(2019, 10, 20), "TF", 8, 01);
		System.out.println("--------------------");
		CloneData.cloneTicketID(LocalDate.of(2019, 10, 10), LocalDate.of(2019, 10, 20), "PC", 8, 20);

		System.out.println("--------------------");

		// test clone ticket code
		LinkedList<String> barcodes = new LinkedList<String>();

		System.out.println("TF Ticket");
		barcodes.add("aaaaaaaa");
		barcodes.add("bbbbbbbb");
		barcodes.add("abababab");
		barcodes.add("abcabcab");
		barcodes.add("dddddddd");
		barcodes.add("fifififi");
		barcodes.add("aaaabbbb");
		barcodes.add("gggggggg");
		cloneTicketCodeByBarCode(barcodes);
		barcodes.clear();

		System.out.println("--------------------");

		System.out.println("oneway Ticket");
		barcodes.add("cccccccc");
		barcodes.add("dddddddd");
		barcodes.add("cdcdcdcd");
		barcodes.add("ccccdddd");
		barcodes.add("cccdddcc");
		barcodes.add("ccddccdd");
		barcodes.add("ddddcccc");
		barcodes.add("dcdcdcdc");
		cloneTicketCodeByBarCode(barcodes);
		barcodes.clear();

		System.out.println("--------------------");

		// test clone card code
		barcodes.add("NMNMNMNM");
		barcodes.add("MMMMMMMM");
		barcodes.add("NNNNNNNN");
		barcodes.add("SASASASA");
		barcodes.add("GBGBGBGB");
		barcodes.add("FGFGFGFG");
		barcodes.add("ERERERER");
		barcodes.add("EEEERRRR");
		cloneCardCodeByBarCode(barcodes);

		/*
		 * data cloned: OW201910110020 OW201910120021 OW201910140022 OW201910170023
		 * OW201910190024 OW201910130025 OW201910100026 OW201910180027
		 * -------------------- TF201910140001 TF201910190002 TF201910110003
		 * TF201910140004 TF201910180005 TF201910190006 TF201910150007 TF201910100008
		 * -------------------- PC201910190020 PC201910190021 PC201910160022
		 * PC201910180023 PC201910190024 PC201910120025 PC201910130026 PC201910120027
		 * -------------------- TF Ticket aaaaaaaa, 3dbe00a167653a1a bbbbbbbb,
		 * 810247419084c82d abababab, 46c9e2ad5b69bffd abcabcab, 848d93ed4ee299c4
		 * dddddddd, ef800207a3648c7c fifififi, bf47e730aa1f332d aaaabbbb,
		 * c622054d9e6f17b4 gggggggg, 9c72446df124ddf2 -------------------- oneway
		 * Ticket cccccccc, bee397acc400449e dddddddd, ef800207a3648c7c cdcdcdcd,
		 * 3d403bec6086580c ccccdddd, 4faddd8d57ccac53 cccdddcc, eff3fde7bd339c10
		 * ccddccdd, 237c9f7bc06d1ad8 ddddcccc, 6a77292a77c512b0 dcdcdcdc,
		 * 20ff5ee5ed9f9bcf -------------------- NMNMNMNM, ce8088fa59498d1a MMMMMMMM,
		 * f05376020754754c NNNNNNNN, 46374f63a5d96dc0 SASASASA, ea6844318aa97fcf
		 * GBGBGBGB, 936b4877b5d2ca10 FGFGFGFG, bdf027e7da8daf15 ERERERER,
		 * 812aec7102107761 EEEERRRR, ce779ddf85d9dabc
		 * 
		 * 
		 */

	}
}
