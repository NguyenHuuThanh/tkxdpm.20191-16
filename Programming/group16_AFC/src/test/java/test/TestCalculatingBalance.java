package test;

import junit.framework.TestCase;
import utils.CalculatingBalanceVND;

import org.junit.jupiter.api.Test;

import com.group16.afc.model.Station;

/**
 * 
 * @author thanh.nq
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package test
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description test calculate balance
 */
public class TestCalculatingBalance extends TestCase {
	@Test
	public void testCalculateBalance1() {
		CalculatingBalanceVND test = CalculatingBalanceVND.getInstance();
		Station inStation = new Station();
		Station outStation = new Station();
		inStation.setDistance(5.0);
		outStation.setDistance(16.1);
		assertEquals(18000.0, test.calculateBalance(inStation, outStation));
	}
	
	@Test
	public void testCalculateBalance2() {
		CalculatingBalanceVND test = CalculatingBalanceVND.getInstance();
		Station inStation = new Station();
		Station outStation = new Station();
		inStation.setDistance(5.0);
		outStation.setDistance(18.1);
		assertEquals(20000.0, test.calculateBalance(inStation, outStation));
	}

	@Test
	public void testCalculateBalance3() {
		CalculatingBalanceVND test = CalculatingBalanceVND.getInstance();
		Station inStation = new Station();
		Station outStation = new Station();
		inStation.setDistance(5.0);
		outStation.setDistance(10.0);
		assertEquals(10000.0, test.calculateBalance(inStation, outStation));
	}
	
	@Test
	public void testCalculateBalance4() {
		CalculatingBalanceVND test = CalculatingBalanceVND.getInstance();
		Station inStation = new Station();
		Station outStation = new Station();
		inStation.setDistance(5.0);
		outStation.setDistance(10.1);
		assertEquals(12000.0, test.calculateBalance(inStation, outStation));
	}
	
	@Test
	public void testCalculateBalance5() {
		CalculatingBalanceVND test = CalculatingBalanceVND.getInstance();
		Station inStation = new Station();
		Station outStation = new Station();
		inStation.setDistance(5.0);
		outStation.setDistance(5.0);
		assertEquals(0D, test.calculateBalance(inStation, outStation));
	}
}
