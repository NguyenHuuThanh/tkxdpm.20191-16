package test;

import org.junit.jupiter.api.Test;

import com.group16.afc.manager.AFCManagerTicketOneWayIn;
import com.group16.afc.manager.AFCState;

import junit.framework.TestCase;


/**
 * 
 * @author thang.nv
 * <p>date Dec 6, 2019
 * <p>project group16_AFC
 * <p>package test
 * <p>lecturer Nguyen Thi Thu Trang
 * <p>description test OW
 */
public class TestAFCManagerTicketOneWayIn extends TestCase {
	@Test
	public void testGetInforTicket() {
		/*
		cccccccc, bee397acc400449e
		dddddddd, ef800207a3648c7c
		cdcdcdcd, 3d403bec6086580c
		ccccdddd, 4faddd8d57ccac53
		cccdddcc, eff3fde7bd339c10
		ccddccdd, 237c9f7bc06d1ad8
		ddddcccc, 6a77292a77c512b0
		dcdcdcdc, 20ff5ee5ed9f9bcf
		 */
		
		String barcode = "ccccccdc";
		AFCManagerTicketOneWayIn afcManager = new AFCManagerTicketOneWayIn();
		afcManager.setBarcode(barcode);
		afcManager.validateTicket();
		assertEquals(AFCState.BARCODE_VALID, afcManager.getState());
		
		String barcode1 = "dddddDde";
		AFCManagerTicketOneWayIn afcManager1 = new AFCManagerTicketOneWayIn();
		afcManager1.setBarcode(barcode1);
		afcManager1.validateTicket();
		assertEquals(AFCState.BARCODE_INVALID, afcManager1.getState());
	}

}
