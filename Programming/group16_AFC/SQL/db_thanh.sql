-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`point`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`point` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `distance` DOUBLE NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`afc`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`afc` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `direction` TINYINT(4) NOT NULL,
  `point_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_AFC_point1_idx` (`point_id` ASC),
  CONSTRAINT `fk_AFC_point1`
    FOREIGN KEY (`point_id`)
    REFERENCES `mydb`.`point` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`card`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`card` (
  `id` VARCHAR(14) NOT NULL,
  `code` VARCHAR(16) NOT NULL,
  `timeBuy` DATETIME NOT NULL,
  `cash` INT(10) NOT NULL,
  `status` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `Code_UNIQUE` (`code` ASC),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`ticket24h`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`ticket24h` (
  `id` VARCHAR(14) NOT NULL,
  `code` VARCHAR(16) NOT NULL,
  `timeBuy` DATETIME NOT NULL,
  `startTime` DATETIME NULL DEFAULT NULL,
  `expTime` DATETIME NULL DEFAULT NULL,
  `status` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`ticketoneway`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`ticketoneway` (
  `id` VARCHAR(14) NOT NULL,
  `code` VARCHAR(16) NOT NULL,
  `timeBuy` DATETIME NOT NULL,
  `status` VARCHAR(45) NOT NULL,
  `inPoint_id` INT(11) NOT NULL,
  `outPoint_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ticketOneWay_point1_idx` (`inPoint_id` ASC),
  INDEX `fk_ticketOneWay_point2_idx` (`outPoint_id` ASC),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC),
  CONSTRAINT `fk_ticketOneWay_point1`
    FOREIGN KEY (`inPoint_id`)
    REFERENCES `mydb`.`point` (`id`),
  CONSTRAINT `fk_ticketOneWay_point2`
    FOREIGN KEY (`outPoint_id`)
    REFERENCES `mydb`.`point` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`travel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`travel` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `typeTicket` INT(11) NULL DEFAULT NULL,
  `ticket24h_id` VARCHAR(14) NULL DEFAULT NULL,
  `ticketOneWay_id` VARCHAR(14) NULL DEFAULT NULL,
  `Card_id` VARCHAR(14) NULL DEFAULT NULL,
  `inPoint_id` INT(11) NULL DEFAULT NULL,
  `outPoint_id` INT(11) NULL DEFAULT NULL,
  `startTime` DATETIME NOT NULL,
  `endTime` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_travel_ticket24h_idx` (`ticket24h_id` ASC),
  INDEX `fk_travel_Card1_idx` (`Card_id` ASC),
  INDEX `fk_travel_ticketOneWay1_idx` (`ticketOneWay_id` ASC),
  INDEX `fk_travel_point1_idx` (`inPoint_id` ASC),
  INDEX `fk_travel_point2_idx` (`outPoint_id` ASC),
  CONSTRAINT `fk_travel_Card1`
    FOREIGN KEY (`Card_id`)
    REFERENCES `mydb`.`card` (`id`),
  CONSTRAINT `fk_travel_point1`
    FOREIGN KEY (`inPoint_id`)
    REFERENCES `mydb`.`point` (`id`),
  CONSTRAINT `fk_travel_point2`
    FOREIGN KEY (`outPoint_id`)
    REFERENCES `mydb`.`point` (`id`),
  CONSTRAINT `fk_travel_ticket24h`
    FOREIGN KEY (`ticket24h_id`)
    REFERENCES `mydb`.`ticket24h` (`id`),
  CONSTRAINT `fk_travel_ticketOneWay1`
    FOREIGN KEY (`ticketOneWay_id`)
    REFERENCES `mydb`.`ticketoneway` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
