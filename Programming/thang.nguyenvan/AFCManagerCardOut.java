package com.group16.afc.manager;

import java.time.LocalDateTime;

import com.group16.afc.database.MysqlConnect;
import com.group16.afc.model.AFC;
import com.group16.afc.model.PrepaidCard;
import com.group16.afc.model.PrepaidCardStatus;

import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.recognizer.TicketRecognizer;
import hust.soict.se.scanner.CardScanner;

public class AFCManagerCardOut extends AFCManager {

	@Override
	public boolean validateTicket() {
		try {
			code = TicketRecognizer.getInstance().process(barcode);
			if (code != null) {
				setState(AFCState.BARCODE_VALID);
				System.out.println(getState());
				return true;
			}
			
		} catch (InvalidIDException e) {
			e.printStackTrace();
			setState(AFCState.BARCODE_INVALID);
			System.out.println(getState());
			return false;
		}
		setState(AFCState.BARCODE_INVALID);
		System.out.println(getState());
		return false;
		
	}

	@Override
	public boolean getInforTicket() {
		if (getState() == AFCState.BARCODE_VALID) {
			PrepaidCard t  = MysqlConnect.getInstance().getPrepaidCard(code);
			System.out.println(t);
			if (t != null) {
				ticket = t;
				setState(AFCState.GET_INFO_SUCCESS);
				System.out.println(getState());
				return true;
			} 
		}
		setState(AFCState.GET_INFO_FAIL);
		System.out.println(getState());
		return false;
	}

	@Override
	public boolean isAvailableTicket() {
		if(getState() == AFCState.GET_INFO_SUCCESS) {
			PrepaidCard t  = (PrepaidCard) ticket;
			if (t!= null) {
				System.out.println(t);
				if (t.getStatus().compareTo(PrepaidCardStatus.NEW_TRAVEL) == 0) {
					setState(AFCState.AVAIABLE_TICKET);
					System.out.println(getState());
					return true;
				}
			}
		}
		setState(AFCState.UNAVAIABLE_TICKET);
		System.out.println(getState());
		return false;
		
	}
	
	@Override
	public boolean updateInfoTicket() {
		if(getState() == AFCState.AVAIABLE_TICKET) {
			PrepaidCard t = (PrepaidCard) ticket;
			try {
				MysqlConnect.getInstance().updateCardStatus(t.getId(), PrepaidCardStatus.END_TRAVEL.toString());
				MysqlConnect.getInstance().updateCardCash(t.getId(), t.getCash() );
				setState(AFCState.UPDATE_SUCCESS);
				System.out.println(getState());
				return true;
			}catch (Exception e){
				e.printStackTrace();
				setState(AFCState.UPDATE_FAIL);
				System.out.println(getState());
				return false;
			}
			
		}
		setState(AFCState.UPDATE_FAIL);
		System.out.println(getState());
		return false;
	}

	@Override
	public boolean controlGate() {
		if (getState() == AFCState.UPDATE_SUCCESS) {
			GateManager.process();
			setState(AFCState.DONE);
			System.out.println(getState());
		}
		return false;
	}

	@Override
	public void processTicket() {
		validateTicket();
		getInforTicket();
		isAvailableTicket();
		updateInfoTicket();
		controlGate();
	}

	public static void main(String[] args) {
		AFCManagerCardOut afc = new AFCManagerCardOut();
		afc.setBarcode("abababab");
		afc.processTicket();
	}

	public PrepaidCard getCardInfo(String barCode) throws InvalidIDException {
		PrepaidCard card = new PrepaidCard();
		card = MysqlConnect.getInstance().getPrepaidCard(CardScanner.getInstance().process(barCode).toString());
		card.setNewestTravel(MysqlConnect.getInstance().getTravel(card.getId()));
		return card;
	}
	
	public void createNewCardTravel(PrepaidCard card) {
		MysqlConnect.getInstance().insertNewTravel(card.getId(),
				afc.getStation().getId(), LocalDateTime.now());
		MysqlConnect.getInstance().updateCardStatus(card.getId(),
				PrepaidCardStatus.NEW_TRAVEL.toString());
	}
	
	public void updateCardInfo(PrepaidCard card, double cash) {
		MysqlConnect.getInstance().updateTravelEndTime(LocalDateTime.now(),
				card.getNewestTravel().getId());
		MysqlConnect.getInstance().updateCardStatus(card.getId(),
				PrepaidCardStatus.END_TRAVEL.toString());
		MysqlConnect.getInstance().updateCardCash(card.getId(), cash);
	}
}
