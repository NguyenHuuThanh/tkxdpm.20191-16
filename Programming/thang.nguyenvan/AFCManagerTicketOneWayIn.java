package com.group16.afc.manager;

import java.time.LocalDateTime;

import com.group16.afc.database.MysqlConnect;
import com.group16.afc.model.PrepaidCard;
import com.group16.afc.model.PrepaidCardStatus;
import com.group16.afc.model.Ticket24h;
import com.group16.afc.model.TicketOneWay;
import com.group16.afc.model.TicketOneWayStatus;

import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.recognizer.TicketRecognizer;

public class AFCManagerTicketOneWayIn extends AFCManager {

	@Override
	public boolean validateTicket() {
		try {
			code = TicketRecognizer.getInstance().process(barcode);
			if (code != null) {
				setState(AFCState.BARCODE_VALID);
				System.out.println(getState());
				return true;
			}
			
		} catch (InvalidIDException e) {
			e.printStackTrace();
			setState(AFCState.BARCODE_INVALID);
			System.out.println(getState());
			return false;
		}
		setState(AFCState.BARCODE_INVALID);
		System.out.println(getState());
		return false;
		
	}

	@Override
	public boolean getInforTicket() {
		if (getState() == AFCState.BARCODE_VALID) {
			TicketOneWay t  = MysqlConnect.getInstance().getTicketOW(code);
			System.out.println(t);
			if (t != null) {
				ticket = t;
				setState(AFCState.GET_INFO_SUCCESS);
				System.out.println(getState());
				return true;
			} 
		}
		setState(AFCState.GET_INFO_FAIL);
		System.out.println(getState());
		return false;
	}

	@Override
	public boolean isAvailableTicket() {
		if(getState() == AFCState.GET_INFO_SUCCESS) {
			TicketOneWay t  = (TicketOneWay) ticket;
			if (t!= null) {
				System.out.println(t);
				if (t.getStatus().compareTo(TicketOneWayStatus.NEW) == 0) {
					setState(AFCState.AVAIABLE_TICKET);
					System.out.println(getState());
					return true;
				}
					
			}
		}
		setState(AFCState.UNAVAIABLE_TICKET);
		System.out.println(getState());
		return false;
		
	}
	
	@Override
	public boolean updateInfoTicket() {
		if(getState() == AFCState.AVAIABLE_TICKET) {
			TicketOneWay t = (TicketOneWay) ticket;
			try {
				MysqlConnect.getInstance().updateTicketOWStatus(t.getId(), TicketOneWayStatus.USING.toString());
				setState(AFCState.UPDATE_SUCCESS);
				System.out.println(getState());
				return true;
			}catch (Exception e){
				e.printStackTrace();
				setState(AFCState.UPDATE_FAIL);
				System.out.println(getState());
				return false;
			}
			
		}
		setState(AFCState.UPDATE_FAIL);
		System.out.println(getState());
		return false;
	}

	@Override
	public boolean controlGate() {
		if (getState() == AFCState.UPDATE_SUCCESS) {
			GateManager.process();
			setState(AFCState.DONE);
			System.out.println(getState());
		}
		return false;
	}

	@Override
	public void processTicket() {
		validateTicket();
		getInforTicket();
		isAvailableTicket();
		updateInfoTicket();
		controlGate();
	}

	public static void main(String[] args) {
		AFCManagerTicketOneWayIn afc = new AFCManagerTicketOneWayIn();
		afc.setBarcode("abababab");
		afc.processTicket();
	}

	public TicketOneWay getTicketInfo(String barCode) throws InvalidIDException {
		   return	MysqlConnect.getInstance().getTicketOW(TicketRecognizer.getInstance().process(barCode).toString());
		}
		
		public void updateTicketInfo(TicketOneWay ticketOneWay) {
			MysqlConnect.getInstance().updateTicketOWStatus(ticketOneWay.getCode(),
					ticketOneWay.getStatus().toString());
		}
}
