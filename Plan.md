# Tuần 1 
## HW01 - AVM

|Thành viên|Nhiệm vụ|Review|
|----------|--------|------|
|Nguyễn Quang Thành|UC001 - Mua vé 1 chiều, UC002 - Mua vé 24h | Nguyễn Hữu Thắng|
| Nguyễn Hữu Thắng|UC002 - Mua vé 24h, UC003 - Mua thẻ | Phạm Thị Thảo|
| Phạm Thị Thảo |UC003 - Mua thẻ, UC004 - Nạp thẻ |Nguyễn Văn Thắng|
|Nguyễn Văn Thắng| UC004 - Nạp thẻ, UC005 -Mua thẻ|Nguyễn Quang Thành|

## HW01 - AFC

|Thành viên|Nhiệm vụ|Review|
|----------|--------|------|
|Nguyễn Quang Thành|UC006 - Soát vé vào 1 chiều, UC007 Soát vé vào 24h | Nguyễn Hữu Thắng|
| Nguyễn Hữu Thắng|UC008 - Soát vé ra 1 chiều, UC009 - Soát vé ra 24h | Phạm Thị Thảo|
| Phạm Thị Thảo |UC010 - Soát thẻ vào, UC011 - Soát thẻ ra |Nguyễn Văn Thắng|
|Nguyễn Văn Thắng|UC011 - Soát thẻ ra, UC006 - Soát vé vào 1 chiều |Nguyễn Quang Thành|

# Tuần 2
## HW02 - Thiết kế kiến trúc
***Nhiệm vụ: vẽ biểu đồ tương tác các UC được phân công, đồng thời thiết kế các lớp tổng quan***

|Thành viên|Nhiệm vụ|Review|
|----------|--------|------|
|Nguyễn Quang Thành|UC006 - Soát vé vào 1 chiều, UC007 Soát vé vào 24h | Nguyễn Hữu Thắng|
| Nguyễn Hữu Thắng|UC008 - Soát vé ra 1 chiều, UC009 - Soát vé ra 24h | Phạm Thị Thảo|
| Phạm Thị Thảo |UC010 - Soát thẻ vào, UC011 - Soát thẻ ra |Nguyễn Văn Thắng|
|Nguyễn Văn Thắng|UC011 - Soát thẻ ra, UC006 - Soát vé vào 1 chiều |Nguyễn Quang Thành|

# Tuần 3
## HW03 - Thiết kế chi tiết

| Thành viên | Nhiệm vụ | Review |
|----------|--------|------|
| Nguyễn Quang Thành | Thiết kế lớp chi tiết  | Nguyễn Hữu Thắng|
| Nguyễn Hữu Thắng| Thiết kế GUI và biểu đồ chuyển màn hình | Phạm Thị Thảo & Nguyễn Văn Thắng |
| Phạm Thị Thảo & Nguyễn Văn Thắng | vẽ biểu đồ ER-Diagram và thiết kế CSDL | Nguyễn Quang Thành |


# Tuần 4

# Thay đổi yêu cầu

vì tuần này Nguyễn Hữu Thắng sốt xuất huyết nên phân công 2 người là Thành và Văn Thắng làm bài